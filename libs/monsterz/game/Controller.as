﻿package monsterz.game {
	
	import flump.display.Movie;
	import flump.display.Library;
	import starling.events.Event;
	import starling.core.Starling;

	
	public class Controller extends Service{
		
		
		public function failGame(movie:Movie, frameDelay:uint = 0):OutroAnimation{
			globals.gameActive = false;
			signals.failed.dispatch();
			signals.gameplayFinished.dispatch();
			return new OutroAnimation(movie, frameDelay);
		}
		
		
		public function failGameWithoutOutro(delay:Number = 0):void{
			globals.gameActive = false;
			signals.failed.dispatch();
			signals.gameplayFinished.dispatch();
			Jugglers.instance.main.delayCall( completeOutro, delay);
		}
		
		
		public function passGame(score:Number, movie:Movie, frameDelay:uint = 0):OutroAnimation{
			globals.gameBase.time_remaining = globals.timeBar.timeRemaining;
			globals.gameActive = false;
			globals.gameBase.score = score;
			signals.passed.dispatch();
			signals.gameplayFinished.dispatch();
			return new OutroAnimation(movie, frameDelay);
		}
		
		
		public function reinatiatePauseableJuggler(){
			Jugglers.instance.main.add(Jugglers.instance.pauseable);
		}

		
		////////////////////////////////////////////////////////
		//
		// Private
		//
		///////////////////////////////////////////////////////
		
		private var signals:Signals = Signals.instance;
		private var globals:Globals = Globals.instance;
		private var lib:Library = Lib.instance;


		public function Controller(){
			signals.passed.addOnce(function(){
				signals.complete.addOnce( globals.gameBase.pass );
			});
			
			signals.failed.addOnce(function(){
				signals.complete.addOnce( globals.gameBase.fail );
			});
			
			Starling.current.root.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		
		internal function timeElapsed(){
			signals.deactivated.dispatch();
			signals.timeLapsed.dispatch();
		}
		
		
		internal function timeBegin(){
			signals.gameplayStarted.dispatch();
		}
		
		
		private function onEnterFrame(e:Event){
			signals.update.dispatch();
			if(globals.gameActive) signals.pauseableUpdate.dispatch();
		}
		
		
		internal function intializationComplete(){
			globals.gameActive = false;
			Globals.instance.game.init();
			new IntroAnimation(Lib.instance.createMovie("Intro"));
		}
		
		
		internal function unpause(){
			globals.gameBase.unPause();
		}
		
		
		internal function pause(){
			globals.gameBase.pause();
		}
		
		
		internal function completeIntro(){
			globals.gameActive = true;
			Globals.instance.timeBar.start();
		}
		
		
		internal function completeOutro(){
			signals.complete.dispatch();
			/*
			if(globals.gameBase.is_standalone){
				globals.gameBase.destroy();
			}
			*/
		}
		
		
		
		////////////////////////////////////////////////////////
		//
		// Singleton
		//
		////////////////////////////////////////////////////////

		private static var _instance:Controller;
		public static function get instance():Controller{
			if(_instance == null) _instance = new Controller();
			return _instance;
		}
		
		override protected function destroy(){
			_instance = null;
			Starling.current.root.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		

	}
	
}
