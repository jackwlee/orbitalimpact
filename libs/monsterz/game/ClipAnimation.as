﻿package monsterz.game {
	
	import starling.display.Sprite;
	import starling.animation.Tween;
	import flump.display.Movie;
	import starling.display.Quad;
	import flash.display.DisplayObject;
	
	
	internal class ClipAnimation {
		
		private var clip:Sprite;
		private var onFadeComplete:Function = function(){}
		protected var removeOnComplete:Boolean = false;
		
		
		public function ClipAnimation(movie:Movie, callback:Function, frameDelay:uint = 0, noFade:Boolean = false) {
			Utils.frameDelay(frameDelay, show);
			clip = new Sprite();
			
			new SoundPlayer(movie);
			
			function show(){
				Jugglers.instance.main.add(movie);
				
				Globals.instance.clip.addChild(clip);
				clip.addChild(movie);
				
				movie.playOnce();
				
				if(!noFade){
					clip.alpha = 0;
					var tween:Tween = new Tween(clip, 0.3);
					tween.animate("alpha", 1);
					tween.onComplete = onFadeComplete
					
					Jugglers.instance.main.add(movie);
					Jugglers.instance.main.add(tween);
				}else{
					onFadeComplete();
				}
				
				function onLabelPassed(label:String){
					if(label == Movie.LAST_FRAME){
						movie.labelPassed.disconnect(onLabelPassed);
						if(removeOnComplete){
							Globals.instance.clip.removeChild(clip);
						}
						Jugglers.instance.main.remove(movie);
						callback();
					}
				}
				
				movie.labelPassed.connect(onLabelPassed);
			}
		}
		
		
		public function removeOnFullFade(display:DisplayObject){
			onFadeComplete = function(){
				display.parent.removeChild(display);
			}
		}
		
		
		public function addBackground(colour:uint){
			var stageWidth:Number = Globals.instance.root.stage.stageWidth;
			var stageHeight:Number = Globals.instance.root.stage.stageHeight;
			var scaleFactor:Number = Globals.instance.scaleFactor;
			
			var background:Quad = new Quad(stageWidth/scaleFactor, stageHeight/scaleFactor, colour);
			background.x -= background.width / 2;
			background.y -= background.height / 2;
			
			clip.addChildAt(background, 0);
		}

	}
	
}
