﻿package monsterz.game {
	
	import starling.display.Sprite;
	import jackwlee.button.Button;
	import monsterz.game.Lib;
	import monsterz.game.Globals;
	import flump.display.Movie;
	import jackwlee.button.ButtonDelegate;
	import jackwlee.button.MovieButtonDisplay;
	import starling.display.Sprite;
	import treefortress.sound.SoundAS;

	
	public class PauseButton extends Sprite implements ButtonDelegate{

		private var button:Button;
		
		
		public function PauseButton() {
			var display:Movie = Lib.instance.createMovie("PauseButton");
			addChild(display);
			
			x = (Globals.instance.root.stage.stageWidth / Globals.instance.scaleFactor) - (29/2);
			y = 32/2;
			
			button = new Button(display, this, new MovieButtonDisplay());
			button.pressed.add(onButtonPressed);
			
			Signals.instance.destroyGame.addOnce( button.destroy );
			Signals.instance.failed.add( disable );
			Signals.instance.passed.add( disable );
		}

		
		private function disable(){
			button.enabled = false;
		}
		
		
		private function onButtonPressed(){
			var globals:Globals = Globals.instance;
		
			if(globals.gameBase.standalone){
				if(globals.gameBase.paused == false){
					SoundAS.pauseAll();
					globals.gameBase.pause();
				}else{
					SoundAS.resumeAll();
					globals.gameBase.unPause();
				}
			}else{
				if(globals.gameBase.paused) throw("Attempting to unpause with the pause button in non-standalone!");
				SoundAS.pauseAll();
				globals.gameBase.pause();
			}
			
			SoundAS.play("button-up");
		}
		
		
		/////////////////////////////////////////////////
		//
		// ButtonDelegate
		//
		/////////////////////////////////////////////////
		
		public function buttonIsEnabled(button:Button):Boolean{
			if(Globals.instance.gameBase.standalone) return true;
			else return Globals.instance.gameBase.paused == false;
		}
		
		
		public function shouldDestroyButton(button:Button):Boolean{
			return Globals.instance.root.stage == null;
		}

	}
	
}


