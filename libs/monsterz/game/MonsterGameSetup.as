﻿package monsterz.game {
	
	import flash.utils.describeType;
	import monsterz.game.Globals;
	import monsterz.game.ClipAnimation;
	import flump.display.Movie;
	import starling.display.Sprite;
	import starling.core.Starling;
	import flash.events.Event;
	import flash.display.StageScaleMode;
	import starling.display.Quad;
	import treefortress.sound.SoundAS;
	import treefortress.sound.SoundInstance;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	import flash.media.Camera;
	import flash.display.LoaderInfo;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.events.ErrorEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.events.IOErrorEvent;

	
	internal class MonsterGameSetup implements CommandDelegate{

		private var appDomain:ApplicationDomain;
		private var globals:Globals;
		private var rootClass:Class;
		private var backgroundColour:uint;
		private var commonAssets:LoaderInfo;
		private var sounds:Vector.<String>;
		
		
		public function MonsterGameSetup(gameId:String, appDomain:ApplicationDomain, gameBase:MonsterGame, internalConfig:*, rootClass:Class, backgroundColour:uint, sounds:Vector.<String>) {
			globals = Globals.instance;
			globals.gameBase = gameBase;
			globals.config = internalConfig;
			globals.gameId = gameId;
			
			this.appDomain = appDomain;
			this.rootClass = rootClass;
			this.backgroundColour = backgroundColour;
			this.sounds = sounds;
			
			if(gameBase.stage) onAddedToStage(null)
			else gameBase.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			configureExternal(internalConfig);
		}
		
		
		
		private function configureExternal(externalConfig:*){
			trace("Configure external");
			
			var xml:XML = describeType(globals.config);
			
			for each(var node:XML in xml.variable){
				var key:String = node.@name;
				trace("----");
				trace("Internal variable: " + key);
				trace("Internal value " + globals.config[key]);
				trace("External value " + externalConfig[key]);
				if(externalConfig[key] != undefined){
					globals.config[key] = externalConfig[key];
				}
			}
		}
		
		
		public function begin(){			
			if(Globals.instance.gameBase.standalone){
				Starling.current.showStats = true;
			}
			
			if(Globals.instance.gameBase.standalone == false){
				Starling.current.showStats = Globals.instance.gameInit.showStats;
			}
			
			if(Globals.instance.gameBase.standalone == false && Globals.instance.gameBase.settings != null){
				configureExternal( Globals.instance.gameBase.settings );
			}
			
			
			new AssetLoader(this, sounds).execute();
		}
		
		
		public function destroy(){
			Signals.instance.destroyGame.dispatch();
			Service.destroyAllServices();
		}
		
		
		
		public function completed(command:ICommand){
			// Handy shortcuts
			var stageWidth:Number = Starling.current.root.stage.stageWidth;
			var stageHeight:Number = Starling.current.root.stage.stageHeight;
			var scaleFactor:Number = Globals.instance.scaleFactor;
			
			trace("Standalone: " + globals.gameBase.standalone);
			trace("StageWidth: " + stageWidth);
			trace("StageHeight: " + stageHeight);
			trace("ScaleFactor: " + scaleFactor);
			
			
			// Setup root container
			var root:Sprite = Starling.current.root as Sprite;
			globals.root = root;
			root.scaleX = globals.scaleFactor;
			root.scaleY = globals.scaleFactor;
			
			
			// Setup game container
			var game:CommonGameRoot = new rootClass() as CommonGameRoot;
			Utils.centerContainer(game);
			globals.game = game;
			root.addChild(game);
			
			
			// Setup background
			var background:Quad = new Quad(stageWidth/scaleFactor, stageHeight/scaleFactor, backgroundColour);
			background.x -= background.width / 2;
			background.y -= background.height / 2;
			game.addChild(background);
			
			
			// Setup ui container
			var ui:Sprite = new Sprite();
			globals.ui = ui;
			root.addChild(ui);
			
			
			// Setup clip container
			var clip:Sprite = new Sprite();
			Utils.centerContainer(clip);
			globals.clip = clip;
			root.addChild(clip);
			
			
			// Setup pause button
			var pauseButton:Sprite = new PauseButton();
			ui.addChild(pauseButton);
			
			
			// Setup Timebar
			var timebarDelay:uint = Globals.instance.game.timebarDelay;
			var timeBar:TimeBar = new TimeBar(timebarDelay);
			globals.timeBar = timeBar;
			ui.addChild(timeBar);
			
			
			// Remove display object in standalone on destroy
			Signals.instance.destroyGame.add(function(){
				root.parent.removeChild(root);
			});
			
			
			// Setup music
			//var musicInstance:SoundInstance = SoundAS.play("music", 1, 0, 9999, false, true);
			//Signals.instance.gameplayFinished.add( stopSound(musicInstance) );
			
			Signals.instance.destroyGame.add( SoundAS.stopAll );
			Signals.instance.destroyGame.add( SoundAS.removeAll );
			
			
			// Begin
			Controller.instance.intializationComplete();			
		}
		
		
		private function getSound(qualifiedClass:String):Sound{
			try{
				var c:Class = commonAssets.applicationDomain.getDefinition(qualifiedClass) as Class;
				return new c() as Sound;
			}catch (e:Error){
				trace("Could not find sound: " + qualifiedClass + ". Returning null instead.");
				return null;
			}
			return null;
		}
		
		
		private function stopSound(sound:SoundInstance){
			return function(){
				trace("Stop sound");
				sound.stop();
			}
		}
		
		
		
		private function onAddedToStage(e:Event){
			var gameBase:GameBase = Globals.instance.gameBase;
			gameBase.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			if(gameBase.parent == gameBase.root.stage){
				gameBase.stage.scaleMode = StageScaleMode.NO_SCALE;
			}
		}
		

	}
	
}
