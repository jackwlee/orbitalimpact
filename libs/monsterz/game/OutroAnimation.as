﻿package monsterz.game {
	
	import flump.display.Movie;

	
	public class OutroAnimation extends ClipAnimation{


		public function OutroAnimation(movie:Movie, frameDelay:uint = 0){
			super(movie, Controller.instance.completeOutro, frameDelay, false);
		}
		
		
		public static function createById(movieId:String, frameDelay:uint = 0):OutroAnimation{
			var movie:Movie = Lib.instance.createMovie(movieId);
			return new OutroAnimation(movie, frameDelay);
		}

	}
	
}


