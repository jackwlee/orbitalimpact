﻿package monsterz.game{
	
	import org.osflash.signals.Signal;
	
	
	internal class SignalHub extends Service{
		
		private var signals:Vector.<Signal>;
		
	
		final protected function newSignal(... params):Signal{
			if(signals == null) signals = new Vector.<Signal>();			
			var signal:Signal;
			
			switch(params.length){
				case 0:	signal = new Signal();												break;
				case 1:	signal = new Signal(params[0]);										break;
				case 2:	signal = new Signal(params[0], params[1]);							break;
				case 3:	signal = new Signal(params[0], params[1], params[2]);				break;
				case 4:	signal = new Signal(params[0], params[1], params[2], params[3]);	break;
			}
			
			signals.push(signal);
			return signal;
		}
		
		
		
		final public function reset():void{
			for each(var signal:Signal in signals){
				signal.removeAll();
			}
			signals = null;
		}
		
	}
	
}