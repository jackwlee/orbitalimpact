﻿package monsterz.game {
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display3D.Context3DProfile;
	
	/**
	 * ...
	 * @author Code and Visual
	 */
	public class GameInit {
		public var stage3d:Stage3D
		public var stage:Stage
		public var width:Number
		public var height:Number
		public var easter_egg:Boolean
		public var difficulty:int = 1
		public var duration:Number = 20
		public var showStats:Boolean = false;
		public var gpu_profile:String = Context3DProfile.BASELINE_CONSTRAINED;
		public var settings:Object // Refer to GameBase.as for notes about this custom object
		
		public function GameInit() {
		}
	}
}