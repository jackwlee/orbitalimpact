﻿package monsterz.game {
	
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.describeType;
	
	
	public class Service {
		
		private static var destroying:Boolean = false;
		
		
		protected function destroy(){
			throw(new Error("Override Service.destroy and nullify _instance!"));
		}
	
		
		internal static function destroyAllServices(){
			destroying = true;
			while(services.length > 0){
				var service:Service = services.shift();
				service.destroy();
				trace("SERVICE DESTROYED: " + service);
			}
			destroying = false;
		}
		
		
		private static var services:Vector.<Service> = new Vector.<Service>();
		
		public function Service(){
			if(destroying) throw("Attempting to access service " + this + " during the destroy phase!");
			trace("SERVICE CREATED: " + this);

			for each(var toCheck:Service in services){
				if(typeName(toCheck) == typeName(this)){
					throw("Service of type: " + this + "is already added!");
				}
			}
			
			services.push(this);
		}
		
		
		private static function typeName(obj:*):String{
			return flash.utils.describeType(obj).@name;
		}
		

	}
	
}
