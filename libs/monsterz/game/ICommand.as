﻿package monsterz.game {
	
	public interface ICommand {
		
		function execute();

	}
	
}
