﻿package monsterz.game {

	import flump.display.LibraryLoader;
	import flump.executor.Future;
	import flump.display.Library;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flump.executor.FutureTask;
	
	
	public class LibLoader implements ICommand{

		private var delegate:CommandDelegate;
		private var loader:LibraryLoader;
		
		
		public function LibLoader(delegate:CommandDelegate){
			this.delegate = delegate;
		}
		
		
		public function execute(){
			loader = new LibraryLoader();
			var path:String = "assets/" + Globals.instance.gameId + "/flump/scale_"+ Globals.instance.scaleFactor +"/animations.zip";
			trace("Loading: " + path);
			var future:Future = loader.loadURL(path);
			future.completed.connect(onLibraryLoaded);
			future.failed.connect(onLibraryLoadFailed);
		}
		
		
		private function onLibraryLoaded(future:FutureTask){
			var lib:Library = future.result;
			Lib.setInstance(lib);
			delegate.completed(this);
		}
		
		
		private function onLibraryLoadFailed(errorEvent:IOErrorEvent){
			throw(errorEvent.text);
		}

	}
	
}
