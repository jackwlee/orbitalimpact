﻿package monsterz.game {
	
	import starling.core.Starling;
	import starling.animation.Juggler;

	
	public class Jugglers extends Service{

		
		public var main			:Juggler = new Juggler();
		public var pauseable	:Juggler = new Juggler();
		
		
		public function Jugglers(){
			Starling.current.juggler.add(main);
			
			Signals.instance.activated.add( onActivate );
			Signals.instance.deactivated.add( onDeactivate );
		}

		
		private function onActivate(){
			main.add(pauseable);
		}

		
		private function onDeactivate(){
			main.remove(pauseable);
		}
		
		
		override protected function destroy(){
			trace("Jugglers destroy");
			Starling.current.juggler.remove(main);
			main = null;
			pauseable = null;
			_instance = null;
		}
		

		

		/////////////////////////////////////////////////////
		//
		//  Singleton
		//
		/////////////////////////////////////////////////////
		
		
		private static var _instance:Jugglers;
		
		
		public static function get instance():Jugglers{
			if(_instance == null){
				_instance = new Jugglers();
				
			}
			return _instance;
		}

		

	}
	
}
