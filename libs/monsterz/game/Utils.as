﻿package monsterz.game{
	
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import flump.display.Movie;
	import starling.events.Event;
	
	
	public class Utils{

		
		public static function frameDelay(frames:uint, callback:Function, whenActive:Boolean = false){
			if(Globals.instance.gameActive || whenActive == false){
				addHandler();
			}
			
			var count:uint = 0;
			
			function onEnterFrame(e:Event){
				count++;
				if(count >= frames){
					removeHandler();
					callback();
				}
			}
			
			function addHandler(){
				Globals.instance.root.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			
			function removeHandler(){
				Globals.instance.root.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			
			if(whenActive){
				Signals.instance.activated.add(addHandler);
				Signals.instance.deactivated.add(removeHandler);
			}
			
			Signals.instance.destroyGame.addOnce(removeHandler);
		}
		

		public static function onFrameNumber(movie:Movie, frameNumber:uint, callback:Function){
			movie.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			function onEnterFrame(e:Event){
				if(movie.frame >= frameNumber){
					movie.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
					callback();
				}
			}
		}
		
		
		public static function centerContainer(container:DisplayObject){
			var scaleFactor:Number = Globals.instance.scaleFactor;
			container.x = (Starling.current.stage.stageWidth / scaleFactor) / 2;
			container.y = (Starling.current.stage.stageHeight / scaleFactor) / 2;
		}
		
		
		public static function scaleStarling(){
			//var stage = Starling.current
			var stage:Stage = Starling.current.nativeStage;
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			var stageWidth:Number = stage.stageWidth;
			var stageHeight:Number = stage.stageHeight;
			var screenWidth:Number = stage.fullScreenWidth;
			var screenHeight:Number = stage.fullScreenHeight;
			
			trace(stageWidth, stageHeight, screenWidth, screenHeight);
			
			var stageHyp:Number = Math.sqrt((stageWidth*stageWidth) + (stageHeight*stageHeight));
			var screenHyp:Number = Math.sqrt((screenWidth*screenWidth) + (screenHeight*screenHeight));
			
			
			var appModel = {};
			appModel.isIOS = true;
			appModel.isSimulator = true;
	
			var resolution:Number;
			if(appModel.isIOS || appModel.isSimulator){
				resolution = Math.round(resolution);
			if(resolution >= 4) resolution = 4;
			else if(resolution <= 1) resolution = 1;
			else resolution = 2;
			}
			
			
			//var resolution:Number = 2;
			
			
			appModel.resolution = resolution;
			appModel.width = 768/2;
			appModel.height = 1024/2;
			appModel.screenWidth = screenWidth/resolution;
			appModel.screenHeight = screenHeight/resolution;
			
			var s:Starling = Starling.current;
			s.stage.stageWidth = appModel.screenWidth;
			s.stage.stageHeight = appModel.screenHeight;
			//s.showStats = true;
			//s.enableErrorChecking = false;
			//s.antiAliasing = 0;
			//s.stage.color = stageColour;
			
			//s.start();
		}
		
		
	}
}