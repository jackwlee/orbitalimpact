﻿//
//***************************************************************
//
// This is the abstract base class for a Starling minigame in the Monsterz app.
// If you are making a Starling based minigame your document root class should be a subclass of this class
// See monsterz.game.GameBase  for more information.
//
// Functions here are available to be overwritten.
// See Example.as for a very simple usage.
//
//***************************************************************
package monsterz.game {
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import starling.events.Event;
	import flash.system.Capabilities;
	
	/**
	 * ...
	 * @author Code and Visual
	 */
	public class GameBaseStarling extends GameBase {
		//
		// **************************************************************
		//
		//
		// PROPERTIES
		//
		//
		// **************************************************************
		//
		//----------------------------------------------------------------------
		///
		// (!) MUST BE SET BY CHILD CLASS
		//
		// The starling_game_class property is required and is a reference to the class
		// that will be used as your Starling root object.
		// 
		//----------------------------------------------------------------------
		protected var starling_game_class:Class
		protected var starling:Starling;
		
		//
		// **************************************************************
		//
		//
		// CONSTRUCTOR
		//
		//
		// **************************************************************
		public function GameBaseStarling() {
			super()
		}
		
		//
		// **************************************************************
		//
		//
		// INIT
		//
		//
		// **************************************************************	
		
		override protected function initGame():void {
			super.initGame();
			initStarling()
		}
		
		//
		// **************************************************************
		//
		//
		//
		// STARLING
		//
		//
		// **************************************************************
		protected function initStarling():void {
			
			context.configureBackBuffer(active_stage.stageWidth, active_stage.stageHeight, 0, false);
				
			var myPort:Rectangle = new Rectangle(0, 0, context.backBufferWidth, context.backBufferHeight);
			
			if(!Starling.multitouchEnabled){
				Starling.multitouchEnabled = true;
			}
			
			
			starling = new Starling(starling_game_class, active_stage, myPort, stage3d, Context3DRenderMode.AUTO, gpu_profile);
			starling.showStats = true;

			var s:Starling = starling;
			starling.addEventListener(starling.events.Event.ROOT_CREATED, doStarlingReady)
		}
		
		protected function doStarlingReady(e:starling.events.Event):void {
			starling.removeEventListener(starling.events.Event.ROOT_CREATED, doStarlingReady)
			starling.start()
			if (_standalone) {
				this.addEventListener(flash.events.Event.ENTER_FRAME, standaloneEnterframe)
			}
		}
		
		//
		//***************************************************************
		//
		//
		// CYCLE
		//
		//
		//**************************************************************
		override public function update():void {
			super.update()
		}
		override public function render():void {
			super.render()
			starling.nextFrame();
		}		
		
		//
		//***************************************************************
		//
		//
		// DESTROY
		//
		//
		//**************************************************************
		override public function destroy():void{
			trace("Gamebase Starling destroy");
			starling.dispose();
			starling = null;
			super.destroy();
		}
		//
		// **************************************************************
		//
		//
		// SHORTCUTS
		//
		//
		// **************************************************************		
		public function get starling_root():* {
			return Object(starling_game_class).getInstance()
		}
	}
}