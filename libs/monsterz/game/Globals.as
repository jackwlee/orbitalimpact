﻿package monsterz.game{

	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flump.display.Library;
	import starling.display.Sprite;
	import monsterz.game.GameBaseStarling;
	import monsterz.game.GameInit;
	import starling.animation.Juggler;
	import starling.core.Starling;
	
	
	public class Globals extends Service {

		private static var _instance:Globals;
		
		public var root:Sprite;
		public var ui:Sprite;
		public var clip:Sprite;
		public var gameId:String;
		public var timeBar:TimeBar;
		
		public var game:CommonGameRoot;
		
		public var gameBase:GameBase;
		public var config:*;
		public var gameInit:GameInit;
		
		
		private var _gameActive:Boolean = false;
		public function get gameActive():Boolean{
			return _gameActive;
		}
		
		public function set gameActive(value:Boolean){
			if(_gameActive != value){
				_gameActive = value;
				if(_gameActive){
					Signals.instance.activated.dispatch();
				}else{
					Signals.instance.deactivated.dispatch();
				}
			}
		}

		
		public function Globals(){
			Signals.instance.activated.add(function(){
				_gameActive = true;
			});
			
			Signals.instance.deactivated.add(function(){
				_gameActive = false;
			});
		}
		
		
		public function get timeRemaining():Number{
			return timeBar.timeRemaining;
		}
		
		
		public static function get instance():Globals{
			if(_instance == null){
				_instance = new Globals();
			}
			return _instance;
		}
		
		
		public function get resolution():Number{
			return Math.ceil(scaleFactor);
		}
		

		public function get scaleFactor():Number{
			return (Starling.context.backBufferHeight/768)*2;
		}
		
		
		public function get gameHeight():Number{
			return game.stage.stageHeight / scaleFactor;
		}

		
		public function get gameWidth():Number{
			return game.stage.stageWidth / scaleFactor;
		}

		override protected function destroy(){
			trace("Globals destroy");
			gameBase = null;
			root = null;
			ui = null;
			clip = null;
			game = null;
			config = null;
			gameInit = null;
			_instance = null;
		}

	}
	
}
