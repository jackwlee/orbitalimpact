﻿package monsterz.game {
	
	import starling.display.Sprite;
	
	
	public class CommonGameRoot extends Sprite{		

		public var timebarDelay:int = 0; // Number of frames before the timebar shows after intro
		
		public function init(){} // Override me

	}
	
}
