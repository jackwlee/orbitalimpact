﻿package monsterz.game {
	
	import starling.animation.Transitions;

	
	public class PowerTransition {

		private static const registered:Boolean = false;

		
		public static function get POWER():String{
			register();
			return "power";
		}
		
		
		private static function register(){
			if(registered == false){
				Transitions.register("power", function(ratio:Number){
					return easeCombined(powerIn, powerOut, ratio);
				});
			};
		}
		
		
		private static function tanH(e:Number, t:Number):Number{
			var a:Number = Math.pow(e, t) - Math.pow(e, -t);
			var b:Number = Math.pow(e, t) + Math.pow(e, -t);
			return a / b;
		}

		
		private static function powerOut(ratio:Number){
			var invRatio:Number = ratio-1;
			return - (invRatio * invRatio) + 1 ;
		}
		
		
		private static function powerIn(ratio:Number){
			return ratio*ratio;
		}
		
		
		protected static function easeCombined(startFunc:Function, endFunc:Function, ratio:Number):Number{
            if (ratio < 0.5) return 0.5 * startFunc(ratio*2.0);
            else             return 0.5 * endFunc((ratio-0.5)*2.0) + 0.5;
        }

	}
	
}
