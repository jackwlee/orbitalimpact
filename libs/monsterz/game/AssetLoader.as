﻿package monsterz.game {
	
	import flash.events.Event; 
	import flash.media.Sound; 
	import flash.net.URLRequest; 
	import treefortress.sound.SoundAS;
	
	
	public class AssetLoader implements ICommand, CommandDelegate{

		private var delegate:CommandDelegate;
		private var commands:Vector.<ICommand> = new Vector.<ICommand>();
		
		
		public function AssetLoader(delegate:CommandDelegate, sounds:Vector.<String>) {
			this.delegate = delegate;
			 
			commands.push(new LibLoader(this));
			commands.push(new SoundLoader(this, "music", "assets/" + Globals.instance.gameId + "/sounds/music.mp3"));
			commands.push(new SoundLoader(this, "button-down", "assets/common/sounds/button-down.mp3"));
			commands.push(new SoundLoader(this, "button-up", "assets/common/sounds/button-up.mp3"));
			
			for each(var soundId:String in sounds){
				commands.push(new SoundLoader(this, soundId, "assets/" + Globals.instance.gameId + "/sounds/"+ soundId +".mp3"));
			}
		}
		
		
		public function completed(command:ICommand){
			commands.shift();
			execute();
		}
		

		public function execute(){
			if(commands.length == 0) return delegate.completed(this);
			commands[0].execute();
		}
		

	}
	
}
