﻿package monsterz.game {
	
	import starling.animation.Transitions;
	import flash.utils.getTimer;
	
	
	public class CubicTransition {

		private static const registered:Boolean = false;
		
		
		public static function get EASE_IN_CUBIC():String{
			register();
			return "easeInCubic";
		}
		
		
		public static function get EASE_OUT_CUBIC():String{
			register();
			return "easeOutCubic";
		}
		
		
		private static function register(){
			if(registered == false){
				Transitions.register("easeInCubic", function(ratio:Number):Number{
					return ratio * ratio;
				});
			 
				
				Transitions.register("easeOutCubic", function(ratio:Number):Number{
					var invRatio:Number = ratio - 1.0;
					return (invRatio * invRatio * invRatio * invRatio * -1) + 1;
				});
			}
		}
		
	}
	
}
