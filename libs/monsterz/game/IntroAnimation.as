﻿package monsterz.game {
	
	import flump.display.Movie;
	import monsterz.game.Globals;
	import starling.events.TouchEvent;
	import starling.animation.Tween;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	
	
	public class IntroAnimation extends ClipAnimation{
		
		private var movie:Movie;
		
		
		public function IntroAnimation(movie:Movie) {
			super(movie, onIntroComplete, 0, true);
			removeOnComplete = true;
			this.movie = movie;
			movie.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		
		private function onIntroComplete(){
			movie.removeEventListener(TouchEvent.TOUCH, onTouch);
			Controller.instance.completeIntro();
		}

		
		private function onTouch(e:TouchEvent){
			for each(var touch:Touch in e.touches){
				if(touch.phase == TouchPhase.BEGAN){
					movie.removeEventListener(TouchEvent.TOUCH, onTouch);
					movie.touchable = false;
					
					var tween:Tween = new Tween(movie, 0.25);
					tween.animate("alpha", 0);
					movie.stop();
					tween.onComplete = function(){
						movie.parent.removeChild(movie);
						onIntroComplete();
					}
					Jugglers.instance.main.add(tween);
				}
			}
		}
		
		
		public static function createById(movieId:String):IntroAnimation{
			var movie:Movie = Lib.instance.createMovie(movieId);
			return new IntroAnimation(movie);
		}
		
		
	}
	
}
