﻿package monsterz.game {

	import flump.display.Library;
	import flump.executor.Future;
	import flump.display.LibraryLoader;
	import flash.utils.ByteArray;
	

	public class Lib {

		private static var _instance:Library;
		
		
		public static function get instance():Library{
			if(_instance == null) throw("You must call setInstance before attempting to request an instance");
			return _instance;
		}
		
		
		internal static function setInstance(lib:Library){
			_instance = lib;
			Signals.instance.destroyGame.addOnce(destroy);
		}
		
		
		private static function destroy(){
			trace("DESTROYED Lib");
			_instance.dispose();
			_instance = null;
		}
		
		/*
		internal static function load(libClass:Class, delegate:ILib) {
			const loader:Future = LibraryLoader.loadBytes(ByteArray(new libClass()), null, Globals.instance.resolution);
			loader.succeeded.connect(onLibraryLoaded)
			loader.failed.connect( function (e :Error) :void { throw e; });
			
			function onLibraryLoaded(lib:Library){
				Lib._instance = lib;
				delegate.libraryLoaded();
			}
			
			
		}
		*/
	
	}
	
}

