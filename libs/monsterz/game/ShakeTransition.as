﻿package monsterz.game {
	
	import starling.animation.Transitions;
	import flash.utils.getTimer;
	
	
	public class ShakeTransition {

		private static const registered:Boolean = false;
		private static var shakes:int = 0;
		
		
		public static function get SHAKE():String{
			register();
			return "shake";
		}
		
		
		private static function register(){
			if(registered == false){
				Transitions.register("shake", function(ratio:Number){
					
					return ((ratio-1) * Math.cos((ratio*Math.PI) * 20)) + 1;
				});
			};
		}
		
	}
	
}
