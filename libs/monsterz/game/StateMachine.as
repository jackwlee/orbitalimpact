﻿package monsterz.game {
	
	import flump.display.Movie;
	
	
	public class StateMachine {

		private var movie:Movie;
		private var delegate:IStateMachine;
		public var currentState:String;
		
		private var loops:Vector.<String> = new Vector.<String>();
		private var playThroughs:Vector.<String> = new Vector.<String>();
		private var stops:Vector.<String> = new Vector.<String>();
		
		private var signals:Signals = Signals.instance;

		private var stateChanges:uint = 0;
		
		
		public function StateMachine(movie:Movie, delegate:IStateMachine) {
			this.movie = movie;
			this.delegate = delegate;
			movie.labelPassed.connect(onLabelChange);
		}
		

		public function get current():String{
			return currentState;
		}
		
		
		public function loop(state:String){
			loops.push(state);
		}
		
		
		public function playThrough(state:String){
			playThroughs.push(state);
		}
		
		
		public function stop(state:String){
			stops.push(state);
		}
		

		public function change(newState:String){
			stateChanges++;
			if(currentState) delegate.stateExit(currentState);
			currentState = newState;
			if(movie.getFrameForLabel(newState) >= 0){
				movie.goTo(newState);
				
				if(shouldStop(newState)) movie.stop();
				else movie.play();
			}
			
			delegate.stateEnter(currentState);
		}
		
		
		public function changeLater(state:String, force:Boolean = false):Function{
			var numChanges:Number = stateChanges;
			return function(){
				if(numChanges == stateChanges || force){
					change(state);
				}
			}
		}

		
		private function goTo(frame:*){
			
		}
		
		
		private function onLabelChange(label:String){
			if(ignoreLabelChanges) return; 
			if(label == currentState) return;
			if(label.indexOf("sound_") > -1) return;
			
			if(shouldLoop(currentState)){
				movie.goTo(currentState);
				movie.play();
			}else if(shouldPlayThrough(currentState)){
				var numStateChanges:uint = stateChanges;
				delegate.stateExit(currentState);
				if(numStateChanges == stateChanges){
					currentState = label;
					
					var numStateChanges2:uint = stateChanges;
					delegate.stateEnter(label);
					if(numStateChanges2 == stateChanges){
						if(shouldStop(currentState)){
							movie.stop();
						}
					}
				}
			}else{
				var newFrame:int = movie.getFrameForLabel(label) - 1;
				if(newFrame < 0) newFrame = movie.numFrames-1;
				
				movie.goTo(newFrame);
				if(shouldStop(currentState)) movie.stop();
				else movie.play();
				delegate.stateLapse(currentState);
			}
			
		}
		
		
		private function get ignoreLabelChanges():Boolean{
			if(currentState == null) return false;
			return shouldStop(currentState);
		}
		
		
		private function shouldLoop(state:String){
			return loops.indexOf(state) != -1;
		}
		
		
		private function shouldPlayThrough(state:String){
			return playThroughs.indexOf(state) != -1;
		}
		
		
		private function shouldStop(state:String){
			return stops.indexOf(state) != -1;
		}
		

	}
	
}
