﻿//
//***************************************************************
//
// This is the abstract base class for a minigame swf in the Monsterz app
// The app allows for minigames to be made with Starling or with other 
// GPU engines (like Code and Visual's own engine).
// Choose the appropriate subclass for your minigame, or make your own.
//
// i.e. for a Starling minigame use GameBaseStarling.as which extends this class.
//
// The intention is to keep the base classes as lightwieght as possible and to be
// non-prescriptive about how a minigame is structured, as long as it does the following:
//
// - Dispatches GameBase.EVENT_PAUSE and GameBase.EVENT_UNPAUSE events
// - Dispatches GameBase.EVENT_PASS and GameBase.EVENT_FAIL events
// - Uses Stage3D in a sharedContext sense e.g.: http://wiki.starling-framework.org/tutorials/combining_starling_with_other_stage3d_frameworks
// - Waits for the externalInit Function to be called before starting playback when running in the app. This function receives a GameInit instance as it's single argument.
//
//	 GameInit contains standard settings for the game as well as an object named "settings" which contains custom variables for the individual minigame. These settings will 
//   be defined by each game developer for their own games and need to be supplied when ready for the main app to provide them. These should also be used for testing purposes
// 	 in order to tweak and balance the diffculties and game variables.
// 
// OTHER REQUIREMENTS:
//
// SCALING - Minigames must scale with the standard scaling rules that will be used app wide (to be described in documentation supplied). 
// Each minigame must be able to handle scaling to different devices size and proportions, but will only ever be played landscape.
//
// PAUSE BUTTON and GAMER TIMER -These items must be created and controlled by each minigame though their appearance and behaviour should be consistent with other games
// INTRO, PASS and FAIL ANIMATIONS - These are contained within and controlled by each individual minigame.
//
//
//***************************************************************
package monsterz.game {
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProfile;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.system.ApplicationDomain;
	
	/**
	 * ...
	 * @author Code and Visual
	 */
	public class GameBase extends MovieClip {
		//
		// **************************************************************
		//
		//
		// PROPERTIES
		//
		//
		// **************************************************************
		//
		// CONSTS
		public static const NATIVE_WIDTH:Number = 1024
		public static const NATIVE_HEIGHT:Number = 768
		// 
		// OBJECTS
		protected var domain:ApplicationDomain 	// Needs to be set in the constructor of unique child class 
												// for you game that extends this (via GameBaseStarling). Like so:
												// domain = ApplicationDomain.currentDomain
		protected var stage3d:Stage3D
		protected var gpu_profile:String 
		public var difficulty:int = 1 // Game difficulty 1-6
		public var parent_stage:Stage // Stage object of the parent app (if not standalone)
		public var parent_width:Number = NATIVE_WIDTH // Width of playback
		public var parent_height:Number = NATIVE_HEIGHT // Height of playback
		public var easter_egg:Boolean // Specifies if this game instance should activate it's easter egg or not (set via the externalInit function)
		public var duration:Number = 30 // To be updated via the externalInit function
		public var settings:Object = {} // settings is an object sent through as a property on the GameInit object argument via externalInit function. 
										// It contains values speicifially for the individual game and allows the game charactersistics to be tweaked 
										// from the app for testing purposes, to test and determine the correct difficulty settings.
		//
		// FLAGS
		public var paused:Boolean = false
		protected var _standalone:Boolean = true;
		protected var _pretendStandalone:Boolean;
		//
		// EVENTS
		static public const EVENT_PAUSE:String = "event_pause";
		static public const EVENT_UNPAUSE:String = "event_unpause";
		static public const EVENT_PASS:String = "event_pass"
		static public const EVENT_FAIL:String = "event_fail"
		static public const EVENT_EASTER_EGG:String = "event_easter_egg"
		//
		// VALUES
		public var score:Number = 0;
		public var time_remaining:Number;
		//


		
		//
		// **************************************************************
		//
		//
		// CONSTRUCTOR
		//
		//
		// **************************************************************
		public function GameBase() {
			_standalone = is_standalone
			init()
		}
		
		//
		// **************************************************************
		//
		//
		// INIT
		//
		//
		// **************************************************************
		private function init():void {
			trace("GameBase - STANDALONE = " + _standalone)
			if (_standalone) {
				initGPU()
			} else {
				trace("Not running standalone, waiting to be initalised by parent")
			}
		}
		
		//
		//-----------------------------------------------------------------
		// (!) This initialisation function is called by the parent when the game is loaded through the app
		// If it's not running stand alone, this is what will start the game's initialisation.
		//-----------------------------------------------------------------
		public function externalInit(thisInit:GameInit):void {
			stage3d = thisInit.stage3d
			parent_stage = thisInit.stage
			parent_width = thisInit.width
			parent_height = thisInit.height
			difficulty = thisInit.difficulty
			duration = thisInit.duration
			gpu_profile = thisInit.gpu_profile
			easter_egg = thisInit.easter_egg
			settings = thisInit.settings
			initGame()
		}
		
		
		public function externalInitAsStandalone():void{
			_pretendStandalone = true;
			_standalone = is_standalone;
			init();
		}
		
		//
		// **************************************************************
		//
		//
		// GPU
		//
		//
		// **************************************************************
		protected function initGPU():void {
			trace("GPU INIT");
			stage3d = stage.stage3Ds[0];
			stage3d.addEventListener(Event.CONTEXT3D_CREATE, contextReturned);
			stage3d.addEventListener(ErrorEvent.ERROR, contextCreationError);
			if(_standalone){
				stage3d.requestContext3D();
			}else{
				stage3d.requestContext3DMatchingProfiles(Vector.<String>([Context3DProfile.BASELINE, Context3DProfile.BASELINE_EXTENDED]))
			}
		}
		
		protected function contextCreationError(error:ErrorEvent):void {
			trace(error.errorID + ": " + error.text);
			removeStage3DListeners()
		}
		
		protected function contextReturned(e:Event):void {
			gpu_profile = context.profile
			removeStage3DListeners()
			initGame()
		}
		
		protected function removeStage3DListeners():void {
			if (is_standalone) {
				stage3d.removeEventListener(Event.CONTEXT3D_CREATE, contextReturned);
				stage3d.removeEventListener(ErrorEvent.ERROR, contextCreationError);
			}
		}
		
		//
		// **************************************************************
		//
		//
		// GAME
		//
		//
		// **************************************************************
		protected function initGame():void {
			addCycle()
		}
		
		//
		//***************************************************************
		//
		//
		// CYCLE
		//
		// In standalone playback, the game will run standaloneEnterframe on the ENTER_FRAME event
		// This function controls the updating and rendering of the minigame. 
		// When played inside the app update() and render() Will be called by the app instead.
		//
		//***************************************************************
		public function addCycle():void {
			if (_standalone) {
				addEventListener(Event.ENTER_FRAME, standaloneEnterframe)
			}
		}
		
		public function standaloneEnterframe(event:Event = null):void {
			update()
			context.clear()
			render()
			context.present()
		}
		
		public function update():void {
			// To be overwritten
		}
		
		public function render():void {
			// To be overwritten
		}
		
		//
		//***************************************************************
		//
		//
		// PAUSE
		// 
		// Pausing will be triggered by the minigame, with the app listening out for 
		// The EVENT_PAUSE and EVENT_UNPAUSE events. 
		// 
		// FYI, a little quirk, once paused the app will display an overlay over the minigame which 
		// will contain it's own "continue" button (i.e. unpause). In this way, the minigame doesn't actually control/trigger
		// the unpause request directly, but just for sake of symmetry we will simulate the trigger coming from the game
		// by allowing the app to call the game's "unPause" function, which then needs to send the event for the app to remove
		// the pause overlay.
		//
		//
		//***************************************************************
		public function pause():void {
			trace("pause")
			paused = true
			dispatchEvent(new Event(EVENT_PAUSE))
		}
		
		public function unPause():void {
			trace("unpause")
			paused = false;
			dispatchEvent(new Event(EVENT_UNPAUSE))
		}
		
		protected function togglePause():void {
			paused = !paused
			if (paused) {
				pause()
			} else {
				unPause()
			}
		}
		
		//
		//***************************************************************
		//
		//
		// COMPLETE
		//
		//
		//***************************************************************
		public function pass():void {
			dispatchEvent(new Event(EVENT_PASS))
		}
		
		public function fail():void {
			dispatchEvent(new Event(EVENT_FAIL))
		}
		
		//
		// **************************************************************
		//
		//
		// SHORTCUTS
		//
		//
		// **************************************************************

		public function get standalone():Boolean{
			return _standalone || _pretendStandalone;
		}
		
		protected function get is_standalone():Boolean {
			return (root.parent && root.parent == stage) || _pretendStandalone;
		}
		
		public function get context():Context3D {
			return stage3d.context3D
		}
		
		public function get active_stage():Stage {
			return stage || parent_stage
		}
		
		public function get active_width():Number {
			return _standalone ? active_stage.width : parent_width
		}
		
		public function get active_height():Number {
			return _standalone ? active_stage.height : parent_height
		}
		
		//
		// **************************************************************
		//
		//
		// DESTROY
		//
		//
		// **************************************************************
		public function destroy():void {
			trace("GameBase destroy");
			stage3d = null
			parent_stage = null
			settings = null
			if(_standalone){
				removeEventListener(Event.ENTER_FRAME, standaloneEnterframe)
			}
		}
	}
}