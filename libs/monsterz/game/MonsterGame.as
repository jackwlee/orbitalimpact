﻿package monsterz.game {
	
	import starling.events.Event;
	import flash.events.Event;
	import starling.core.Starling;
	import flash.system.System;
	import flash.media.Sound;
	import flash.system.ApplicationDomain;
	

	
	public class MonsterGame extends GameBaseStarling {
	
		
		private var setup:MonsterGameSetup;
		
		
		public function MonsterGame(gameId:String, appDomain:ApplicationDomain, rootClass:Class, config:*, backgroundColour:uint, sounds:Vector.<String>) {
			starling_game_class = MonsterGameRoot;
			setup = new MonsterGameSetup(gameId, appDomain, this, config, rootClass, backgroundColour, sounds);
			super();
		}
		
		
		override public function externalInit(thisInit:GameInit):void {
			Globals.instance.gameInit = thisInit;
			super.externalInit(thisInit);
		}
		
		
		override protected function doStarlingReady(e:starling.events.Event):void {
			super.doStarlingReady(e);
			trace("Do starling ready");
			setup.begin();
		}
		
		
			
		override public function pause():void {
			super.pause();
			Signals.instance.pause.dispatch();
			Signals.instance.deactivated.dispatch();
		}
		
				
		override public function unPause():void {
			super.unPause();
			Signals.instance.unpause.dispatch();
			Signals.instance.activated.dispatch();
		}
		
		
		override public function destroy():void{
			setup.destroy();
			setup = null;
			
			super.destroy();
		}
			
	}
}


