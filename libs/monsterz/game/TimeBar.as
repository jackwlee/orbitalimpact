﻿package monsterz.game {
	
	import starling.display.Sprite;
	import starling.events.Event;
	import org.osflash.signals.Signal;
	import starling.display.Quad;
	import starling.animation.Tween;
	import starling.animation.Transitions;
	import flash.globalization.NumberParseResult;

	
	public class TimeBar extends Sprite{

		private var gameDurationMili:Number;
		private var elapsed:Number = 0;
		private var stopped:Boolean = false;
		private var framesUntilShow:int;
		
		private var backing:Quad;
		private var bar:Quad;
		
		private var scaleFactor:Number;
		
		public var debugSpeed:Number = 1;
		
		
		public function TimeBar(frameDelay:int = 0) {
			framesUntilShow = frameDelay;
			
			this.scaleFactor = Globals.instance.scaleFactor;
			this.gameDurationMili = Globals.instance.gameBase.duration * 1000;
			
			backing = new Quad(Globals.instance.root.stage.stageWidth / scaleFactor, (14/2), 0xffffff);
			backing.alpha = 0.3;
			addChild(backing);
			
			bar = new Quad(backing.width, backing.height, 0xffffff);
			addChild(bar);
			
			hide(true);
			pause();
			
			Signals.instance.pause.add(pause);
			Signals.instance.passed.addOnce(pause);
			Signals.instance.failed.addOnce(pause);

			Signals.instance.gameplayFinished.add(pause);
			
			Signals.instance.destroyGame.addOnce(destroy);
		}
		
		

		public function start(){
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			if(debugSpeed != 1 && Globals.instance.gameBase.standalone == false) throw new Error("WARNING: TIMEBAR SPEED SET TO DEBUG SPEED: " + debugSpeed);
		}
		
		
		public function get timeRemaining():Number{
			return (1-percentElapsed) * Globals.instance.gameBase.duration;
		}
		

		private function pause():void{
			trace("Timebar paused");
			stopped = true;
		}
		
		
		private function unpause():void{
			trace("Timebar unpaused");
			stopped = false;
		}

		
		private function hide(snapAnimation:Boolean = false):void{
			if(snapAnimation){
				this.y = -backing.height;
			}else{
				var tween:Tween = new Tween(this, 0.5, Transitions.EASE_IN);
				tween.animate("y", -backing.height);
				
				Jugglers.instance.pauseable.add(tween);
			}
		}

		
		private function show():void{
			var tween:Tween = new Tween(this, 0.5, Transitions.EASE_OUT);
			tween.animate("y", 0);
			tween.onComplete = unpause;
			
			Jugglers.instance.main.add(tween);
			Controller.instance.timeBegin();
		}
		
		
		public function nowShow(){
			framesUntilShow = 1;
		}
		
		
		private function onEnterFrame(e:Event){
			if(percentElapsed >= 1) return;
			
			if(framesUntilShow == 0){
				show();
			}
			if(framesUntilShow >= 0) framesUntilShow--;
			
			
			if(stopped) return;

			elapsed += (1000/60) * debugSpeed;
			
			bar.x = -(stage.stageWidth/scaleFactor) * percentElapsed;
			if(percentElapsed >= 1) Controller.instance.timeElapsed();
		}
		
		
		public function get percentElapsed():Number{
			return elapsed / gameDurationMili;
		}
		
		
		private function destroy(){
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}

	}
	
}
