﻿package monsterz.game {

	
	public interface IStateMachine {
		
		function stateEnter(state:String);
		function stateExit(state:String);
		function stateLapse(state:String);

	}
	
}
