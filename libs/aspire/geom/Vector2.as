﻿package aspire.geom {
	
	import utils.ObjectPool;

	
	public class Vector2{
		
		/*
		public static const temp1:Vector2 = new Vector2();
		public static const temp2:Vector2 = new Vector2();
		public static const temp3:Vector2 = new Vector2();
		public static const temp4:Vector2 = new Vector2();
		public static const temp5:Vector2 = new Vector2();
		
		private static const temp6:Vector2 = new Vector2();
		private static const temp7:Vector2 = new Vector2();
		*/
			

		
		private var _x:Number;
		private var _y:Number;
		
		
		public function Vector2(x:Number = 0, y:Number = 0){
			_x = x;
			_y = y;
		}
		
		
		public function slerp(other:Vector2, t:Number):void{
			var cosTheta:Number = dot(other);
			
			if(cosTheta >= 1){
				cosTheta = 0.99999;
			}else if(cosTheta <= -1) cosTheta = -0.99999;
			
			var theta:Number = Math.acos(cosTheta);
			var sinTheta:Number = Math.sin(theta);
			
			if(theta <= 0.0000001 || isNaN(theta)){
				theta = 0.0000001;
			}
			
			if (sinTheta <= 0.0000001 || isNaN(sinTheta)){
				sinTheta = 0.0000001;
			}
			
			var w1:Number = Math.sin((1 - t) * theta) / sinTheta;
			var w2:Number = Math.sin(t * theta) / sinTheta;
			
			x *= w1;
			y *= w1;
			
			x += other.x * w2;
			y += other.y * w2;
		}
		
		
		public static function lerp(from:Vector2, to:Vector2, time:Number):Vector2{
			return lerpInto(from, to, time, new Vector2());
		}
		
		
		public static function lerpInto(from:Vector2, to:Vector2, time:Number, out:Vector2):Vector2{
			out.x = ((from.x - to.x) * time) + from.x;
			out.y = ((from.y - to.y) * time) + from.y;
			return out;
		}
		
		
		public function clone(into:Vector2):Vector2{
			into.x = x;
			into.y = y;
			return into;
		}
		
	
		
		public function zero():Vector2{
			_x = 0;
			_y = 0;
			return this;
		}
		
		
		
		public function isZero():Boolean{
			return _x == 0 && _y == 0;
		}
		
		
		
		public function normalize():Vector2{
			if(length == 0)
			{
				_x = 1;
				return this;
			}
			var len:Number = length;
			_x /= len;
			_y /= len;
			return this;
		}
		
		
		
		public function truncate(max:Number):Vector2{
			if(length > max) length = max;
			return this;
		}
		
		
		public function reverse():Vector2{
			_x = -_x;
			_y = -_y;
			return this;
		}
		
		
		public function isNormalized():Boolean{
			return length == 1.0;
		}
		
		
		public function dot(v2:Vector2):Number{
			return _x * v2.x + _y * v2.y;
		}
		
		
		public function cross(v2:Vector2):Number{
			return _x * v2.y - _y * v2.x;
		}
		
		
		public function angleToPosition(position:Vector2){
			var xp:Number = position.x - _x;
			var yp:Number = position.y - _y;
			return Math.atan2(yp, xp);
		}
		
		
		public static function angleBetween(v1:Vector2, v2:Vector2):Number{
			/*
			if(!v1.isNormalized()) v1 = v1.clone(temp6).normalize();
			if(!v2.isNormalized()) v2 = v2.clone(temp7).normalize();
			return Math.acos(v1.dot(v2));
			*/
			const dot :Number = v1.dot(v2);
			const len1 :Number = v1.length;
			const len2 :Number = v2.length;
	
			return Math.acos(dot / (len1 * len2));
		}
		
		
		
		public function sign(v2:Vector2):int{
			return perp.dot(v2) < 0 ? -1 : 1;
		}
		
	
		public function dist(v2:Vector2):Number{
			return Math.sqrt(distSQ(v2));
		}
		
		
		public function distSQ(v2:Vector2):Number{
			var dx:Number = v2.x - x;
			var dy:Number = v2.y - y;
			return dx * dx + dy * dy;
		}
		
		
		public function add(v2:Vector2):Vector2{
			_x = _x + v2.x;
			_y = _y + v2.y;
			return this;
		}
		
	
		public function subtract(v2:Vector2):Vector2{
			_x = _x - v2.x;
			_y = _y - v2.y;
			return this;
		}
		
		
		
		public function multiply(value:Number):Vector2{
			_x = _x * value;
			_y = _y * value;
			return this;
		}
		
		
		
		public function divide(value:Number):Vector2{
			_x = _x / value;
			_y = _y / value;
			return this;
		}
		
	
		
		public function equals(v2:Vector2):Boolean{
			return _x == v2.x && _y == v2.y;
		}
		
		
		public function set x(value:Number):void{
			_x = value;
		}
		
		
		public function get x():Number{
			return _x;
		}
		
		
		public function set y(value:Number):void{
			_y = value;
		}
		
		
		public function get y():Number{
			return _y;
		}
		
		
		
		public function set length(value:Number):void{
			var a:Number = angle;
			_x = Math.cos(a) * value;
			_y = Math.sin(a) * value;
		}
		
		public function get length():Number{
			return Math.sqrt(lengthSQ);
		}
		
		
		
		public function get lengthSQ():Number{
			return _x * _x + _y * _y;
		}
		
	
		
		public function set angle(value:Number):void{
			var len:Number = length;
			_x = Math.cos(value) * len;
			_y = Math.sin(value) * len;
		}
		public function get angle():Number{
			return Math.atan2(_y, _x);
		}

		
		
		
		public function setToHeading(angle:Number):Vector2{
			_x = 0;
			_y = 1;
			this.angle = angle;
			return this;
		}
		
		
		
		public function get perp():Vector2{
			_x = -y;
			_y = x;
			return this;
		}
		
		
		public function toString():String{
			return "[Vector2 (x:" + _x + ", y:" + _y + ")]";
		}
		
	}
}