﻿package components {

	import framework.ecs.IComponent;
	import aspire.geom.Vector2;
	import framework.ecs.Entity;
	import flash.utils.getTimer;
	import flash.globalization.NumberParseResult;
	import math.Arc;
	import entities.Collections;
	import math.Circle;
	import math.PotentialArcs;
	import math.ArcNode;
	import systems.DebugSystem;
	import utils.*;


	public class Orbit implements IComponent {
	
		public var entity: Entity;
		public var tailArcNode: ArcNode;
		public var headArcNode: ArcNode;
		public var createdOn:Number = 0;
		public var distanceTravelled:Number = 0;
		
		public var thrustedOn:Number = 0;
		public var velocity:Number = 1.5;
		public var thrustVelocity:Number = 1.6;
		public var decelerationDistance:Number = 200; // Distance until normal velocity
		
		//public var potencialArcs:PotentialArcs = new PotentialArcs();
		public var adjacentArc:ArcNode;

		
		public function Orbit(entity: Entity) {
			this.entity = entity;
		}
		
		
		
		public function attemptToSetNewHeading():void{
			if(adjacentArc){
				var newHeading:ArcNode = adjacentArc;
				ArcNodePool.__TAKE__(adjacentArc);
				adjacentArc = null;
				tailArcNode = tailArcNode.attach(newHeading);
				thrustedOn = distanceTravelled;
			}
		}

		
		
		public function calculateAdjacentArc(direction:Number):void {
			var orbit:Orbit = this;
			
			if(adjacentArc){
				ArcNodePool.__RELEASE__(adjacentArc);				// RELEASE ARCNODE
				adjacentArc = null;
			}
				
			var trajectoryAngle:Number = target.entity.position.angleToPosition(orbit.entity.position) + (Math.PI/2 * direction);
			

			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2						
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			var TEMP_VEC_3:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			
			
			var angleVec:Vector2 = TEMP_VEC_1.setToHeading(trajectoryAngle);
			angleVec.normalize();
			
			var closestDot:Number = -1;
			var closestTarget:int;
			var found:Boolean = false;
			
			for(var i:int = 0; i < target.adjacent.length; i++){
				var midVec:Vector2 = TEMP_VEC_2.setToHeading(target.midPoints[i]);
				midVec.normalize();
				
				var dot:Number = midVec.dot(angleVec);
				if(dot > 0){
					if(dot > closestDot){
						closestTarget = i;
						closestDot = dot;
						found = true;
					}
				}
			}
			
			
			if(found){
				var startVec:Vector2 = TEMP_VEC_2.setToHeading(target.startPoints[closestTarget]);
				var endVec:Vector2 = TEMP_VEC_3.setToHeading(target.endPoints[closestTarget]);
				var nextTarget:OrbitTarget = target.adjacent[closestTarget];
				
				var startDot:Number = startVec.dot(angleVec);
				var endDot:Number = endVec.dot(angleVec);
				
				var dir:int = startDot > endDot ? 1 : -1;
				//dir *= direction;
				
				adjacentArc = ArcNodePool.__GET__(null, orbit.distanceTravelled);		// GET ARCNODE
				
				var potencialArc:Arc = orbit.getPotencialArc(nextTarget, dir, direction, adjacentArc.arc);
				
				if(potencialArc == null){
					ArcNodePool.__RELEASE__(adjacentArc);								// RELEASE ARCNODE
					adjacentArc = null;
				}else{
					var nodeOfNextTarget:ArcNode = ArcNodePool.__GET__(nextTarget, adjacentArc.startDistance + adjacentArc.arc.length);
					adjacentArc.attach(nodeOfNextTarget);
					nodeOfNextTarget.arc.direction = potencialArc.inverted ? potencialArc.direction*-1 : potencialArc.direction;
					nodeOfNextTarget.continueOnFromPrev();
				}
		
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_3);					// RELEASE VECTOR2
		}
		

		
		public function get currentArcNode():ArcNode{
			return headArcNode.getNodeForDistanceForwards( distanceTravelled );
			//return tailArcNode.getNodeForDistanceBackwards( distanceTravelled ); // Figure this out!
		}
		
		
				
		public function get angle():Number{
			return currentArcNode.arc.angleForDistance(distanceTravelled - currentArcNode.startDistance);
		}
		
		
		public function get target():OrbitTarget{
			return currentArcNode.target;
		}
		
		
		public function setArcNode(arcNode:ArcNode):void{
			headArcNode = arcNode;
			tailArcNode = arcNode.tail;
		}
		
		
		public function newArcNode(target:OrbitTarget):ArcNode{
			var arc:Arc = new Arc();
			
			if(tailArcNode == null){
				tailArcNode = new ArcNode(arc, target, 0);
				headArcNode = tailArcNode;
				tailArcNode.startDistance = 0;
			}else{
				if(tailArcNode.arc.length == 0xffffff) throw new Error("Created an arc node without first setting the length");
				var nextArcNode:ArcNode = new ArcNode(arc, target, tailArcNode.startDistance + tailArcNode.arc.length);
				tailArcNode = currentArcNode.attach(nextArcNode);
			}
			
			
			return tailArcNode;
		}
		
		
		
		public function clipTailArcNodeWithDistance(distance:Number){
			tailArcNode.arc.length = distance - tailArcNode.startDistance;
		}
		
		
		
		public function newCourse(potencialArc:PotentialArcs):void{
			var TEMP_VEC1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			currentArcNode.arc.length = distanceTravelled - currentArcNode.startDistance;
			
			var node:ArcNode = newArcNode(null);
			
			potencialArc.counter.cloneInto(node.arc); // SET THIS TO THE RIGHT DIRECTION!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
			var endPositionOfCurve:Vector2 = node.arc.positionAtDistance(node.arc.length, TEMP_VEC1);
			//var durationOfCurve:Number = node.arc.length; // CHANGE THIS, LINKED TO TIME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
			thrustedOn = distanceTravelled;
			
			node = newArcNode(potencialArc.target);
			node.arc.radius = potencialArc.target.radius;
			node.arc.center.x = potencialArc.target.entity.position.x;
			node.arc.center.y = potencialArc.target.entity.position.y;
			node.arc.startAngle = potencialArc.target.entity.position.angleToPosition(endPositionOfCurve);
			node.arc.direction = 1; // CHANGE THIS LATER TO SPECIFIC DIRECTION!!!!!!!!!!!!!!!!!!!!!!!!!!
			
			Vector2Pool.__RELEASE__(TEMP_VEC1);							// RELEASE VECTOR2
		}
		

		public function getPotencialArc(potencialTarget:OrbitTarget, direction:Number, phase:Number, out:Arc):Arc{
			if(potencialTarget != this.target && this.target != null){
				var x1:Number = this.target.entity.position.x;
				var y1:Number = this.target.entity.position.y;
				var r1:Number = this.target.radius;
				
				var x2:Number = potencialTarget.entity.position.x;
				var y2:Number = potencialTarget.entity.position.y;
				var r2:Number = potencialTarget.radius;
				
				var x3:Number = entity.position.x;
				var y3:Number = entity.position.y;
				var r3:Number = 0.0001;
				
				var arc:Arc;
				
				if(direction * -phase < 0){ // Clockwise
					arc = apollonius( x1,  y1,   r1,  x2,  y2,  r2,  x3,  y3,  r3, 1, out.reset());	// Rule 1
					if(arc) arc.inverted = false;
					if(arc == null){
						direction *= -1;
						arc = apollonius( x1,  y1,  -r1,  x2,  y2, -r2,  x3,  y3, -r3, 1, out.reset());	 // Rule 8
						if(arc) arc.inverted = false;
					}
				}else{ // Counter
					direction *= -1;
					arc = apollonius( x1,  y1,  r1,  x2,  y2, -r2,  x3,  y3,  r3, -1, out.reset());	// Rule 3
					
					if(arc)  arc.inverted = true;
					if(arc == null){
						direction *= -1;
						arc = apollonius( x1,  y1,  -r1,  x2,  y2, r2,  x3,  y3, -r3, -1, out.reset());		// Set Arc.temp2
						if(arc) arc.inverted = false;
					}
				}
				
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  -r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  -r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  -r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  -r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				
				
				if(arc != null){
					arc.startAngle = arc.center.angleToPosition(entity.position);
					arc.direction = direction;
					arc.clipToIntersection(potencialTarget.entity.position.x, potencialTarget.entity.position.y, potencialTarget.radius+0.0001);
					return arc;
				}
			}
			
			return null;
		}
		
		
		
		public function apollonius(x1:Number, y1:Number, r1:Number, x2:Number, y2:Number, r2:Number, x3:Number, y3:Number, r3:Number, direction:Number, out:Arc):Arc {

			// The quadratic equation (1):
			//
			//          0 = (x - x1)² + (y - y1)² - (r ± r1)²
			//          0 = (x - x2)² + (y - y2)² - (r ± r2)²
			//          0 = (x - x3)² + (y - y3)² - (r ± r3)²
			//
			// Use a negative radius to choose a different circle.
			// We must rewrite this in standard form Ar² + Br + C = 0 to solve for r.
			// Per http://mathworld.wolfram.com/ApolloniusProblem.html
		
			// drawApollo(c1.x, c1.y, +c1.r, c2.x, c2.y, +c2.r, c3.x, c3.y, -c3.r); 
			// drawApollo(c1.x, c1.y, +c1.r, c2.x, c2.y, -c2.r, c3.x, c3.y, -c3.r); 
			
			
			var a2:Number = 2 * (x1 - x2),
				b2:Number = 2 * (y1 - y2),
				c2:Number = 2 * (r2 - r1),
				d2:Number = x1 * x1 + y1 * y1 - r1 * r1 - x2 * x2 - y2 * y2 + r2 * r2,
				a3:Number = 2 * (x1 - x3),
				b3:Number = 2 * (y1 - y3),
				c3:Number = 2 * (r3 - r1),
				d3:Number = x1 * x1 + y1 * y1 - r1 * r1 - x3 * x3 - y3 * y3 + r3 * r3;

			// Giving:
			//
			//          x = (b2 * d3 - b3 * d2 + (b3 * c2 - b2 * c3) * r) / (a3 * b2 - a2 * b3)
			//          y = (a3 * d2 - a2 * d3 + (a2 * c3 - a3 * c2) * r) / (a3 * b2 - a2 * b3)
			//
			// Expand x - x1, substituting definition of x in terms of r.
			//
			//     x - x1 = (b2 * d3 - b3 * d2 + (b3 * c2 - b2 * c3) * r) / (a3 * b2 - a2 * b3) - x1
			//            = (b2 * d3 - b3 * d2) / (a3 * b2 - a2 * b3) + (b3 * c2 - b2 * c3) / (a3 * b2 - a2 * b3) * r - x1
			//            = bd / ab + bc / ab * r - x1
			//            = xa + xb * r
			//
			// Where:

			var ab:Number = a3 * b2 - a2 * b3,
				xa:Number = (b2 * d3 - b3 * d2) / ab - x1,
				xb:Number = (b3 * c2 - b2 * c3) / ab;

			// Likewise expand y - y1, substituting definition of y in terms of r.
			//
			//     y - y1 = (a3 * d2 - a2 * d3 + (a2 * c3 - a3 * c2) * r) / (a3 * b2 - a2 * b3) - y1
			//            = (a3 * d2 - a2 * d3) / (a3 * b2 - a2 * b3) + (a2 * c3 - a3 * c2) / (a3 * b2 - a2 * b3) * r - y1
			//            = ad / ab + ac / ab * r - y1
			//            = ya + yb * r
			//
			// Where:

			var ya:Number = (a3 * d2 - a2 * d3) / ab - y1,
				yb:Number = (a2 * c3 - a3 * c2) / ab;

			// Expand (x - x1)², (y - y1)² and (r - r1)²:
			//
			//  (x - x1)² = xb * xb * r² + 2 * xa * xb * r + xa * xa
			//  (y - y1)² = yb * yb * r² + 2 * ya * yb * r + ya * ya
			//  (r - r1)² = r² - 2 * r1 * r + r1 * r1.
			//
			// Substitute back into quadratic equation (1):
			//
			//          0 = xb * xb * r² + yb * yb * r² - r²
			//              + 2 * xa * xb * r + 2 * ya * yb * r + 2 * r1 * r
			//              + xa * xa + ya * ya - r1 * r1
			//
			// Rewrite in standard form Ar² + Br + C = 0,
			// then plug into the quadratic formula to solve for r, x and y.

			var A:Number = xb * xb + yb * yb - 1,
				B:Number = 2 * (xa * xb + ya * yb + r1),
				C:Number = xa * xa + ya * ya - r1 * r1,
				r:Number = (-B - Math.sqrt(B * B - 4 * A * C)) / (2 * A);
			

			if(r < 0 || isNaN(r)) return null;
			
			out.center.x = xa + xb * r + x1;
			out.center.y = ya + yb * r + y1;
			out.radius = r < 0 ? -r : r;
			return out;
		}

		
	}

}

