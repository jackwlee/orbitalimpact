﻿package components {
	
	import framework.ecs.IComponent;
	
	
	public class Life implements IComponent{

		public var total:Number;
		public var remaining:Number;
		public var onComplete:Function;
		
		
		public function Life(total:Number, onComplete:Function = null) {
			this.total = total;
			this.onComplete = onComplete;
		}
		

	}
	
}
