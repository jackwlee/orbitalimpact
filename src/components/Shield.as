﻿package components {
	
	import components.Powerup;
	import entities.Rocket;
	import flump.display.Movie;
	import framework.flump.FlumpLibs;
	import react.Connection;
	import starling.animation.Tween;
	import starling.animation.Transitions;
	
	
	public class Shield extends Powerup{
	
		public var shieldMovie:Movie;
		
		
		public function Shield(rocket:Rocket){
			super(rocket);
		}
		
		
		override protected function onEnabled(){
			shieldMovie = FlumpLibs.instance.game.createMovie("Shield");
			shieldMovie.stop();
			rocket.display.addChild(shieldMovie);
			Jugglers.instance.game.add(shieldMovie);
			
			shieldMovie.alpha = 0.15;
			shieldMovie.scaleX = 0.6;
			shieldMovie.scaleY = 0.6;
			var tween:Tween = new Tween(shieldMovie, 0.3, Transitions.EASE_OUT);
			tween.animate("scaleX", 1);
			tween.animate("scaleY", 1);
			tween.animate("alpha", 0.3);
			Jugglers.instance.game.add(tween);
		}
		
		
		
		override protected function onDisabled(){
			shieldMovie.play();
			shieldMovie.alpha = 0.85;
			var connection:Connection = shieldMovie.labelPassed.connect(function(label:String){
				if(label == Movie.LAST_FRAME){
					connection.close();
					Jugglers.instance.game.remove(shieldMovie);
					shieldMovie.parent.removeChild(shieldMovie);
				}
			});
		}
		
		
		
		public function render(){
			if(shieldMovie) shieldMovie.rotation = -rocket.display.rotation;
		}
	
		
	}

}

