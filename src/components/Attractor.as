﻿package components {
	
	import framework.ecs.IComponent;
	import framework.ecs.Entity;
	import aspire.geom.Vector2;
	
	
	public class Attractor implements IComponent{

		public var entity:Entity;
		public var targetPosition:Vector2;
		public var headingToPosition:Vector2 = new Vector2(0, 1);
		public var attraction:Number;

		
		public function Attractor(entity:Entity, attraction:Number, targetPosition:Vector2) {
			this.entity = entity;
			this.targetPosition = targetPosition;
			this.attraction = attraction;
		}
		
	
	}
	
	
}

