﻿package components {
	
	import starling.display.Sprite;
	import framework.ecs.IComponent;
	import framework.flump.FlumpLibs;
	import starling.display.Image;
	import flump.display.Movie;
	import starling.display.DisplayObject;

	
	public class Display extends Sprite implements IComponent {

		
		public function addImage(symbolId:String):Image{
			var image:Image = FlumpLibs.instance.game.createImage(symbolId);
			addChild(image);
			trace(image);
			return image;
		}
		
		
		public function addMovie(symbolId:String):Movie{
			var movie:Movie = FlumpLibs.instance.game.createMovie(symbolId);
			addChild(movie);
			Jugglers.instance.game.add(movie);
			return movie;
		}
		
		
		public function addDisplayObject(symbolId:String):DisplayObject{
			var displayObject:DisplayObject = FlumpLibs.instance.game.createDisplayObject(symbolId);
			addChild(displayObject);
			if(displayObject is Movie) Jugglers.instance.game.add(displayObject as Movie);
			return displayObject;
		}
		
	}
	
}
