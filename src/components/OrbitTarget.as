﻿package components {
	
	import framework.ecs.IComponent;
	import framework.ecs.Entity;
	import aspire.geom.Vector2;
	import math.Arc;
	import systems.DebugSystem;
	import utils.Vector2Pool;
	import math.ArcNode;
	import utils.ArcNodePool;
	import utils.ArcPool;

	
	public class OrbitTarget implements IComponent{
	
		public var entity:Entity;
		public var captured:Vector.<Orbit> = new Vector.<Orbit>();
		public var radius:Number = 30;
		
		public var adjacent:Vector.<OrbitTarget> = new Vector.<OrbitTarget>();
		public var startPoints:Vector.<Number> = new Vector.<Number>();
		public var midPoints:Vector.<Number> = new Vector.<Number>();
		public var endPoints:Vector.<Number> = new Vector.<Number>();
		
		
		
		
		public function OrbitTarget(entity:Entity){
			this.entity = entity;
		}
		
		
		
		public function calculateAnglesToAdjacent():void{
			while(startPoints.length > 0) startPoints.pop();
			while(midPoints.length > 0) midPoints.pop();
			while(endPoints.length > 0) endPoints.pop();
			
			var i:int = 0;
			for each(var adj:OrbitTarget in adjacent){
				midPoints[i] = entity.position.angleToPosition(adj.entity.position);
				i++;
			}

			var TEMP_VEC1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC3:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			
			i = 0;
			for each(var midPoint:Number in midPoints){				
				var nextMidPoint:Number = midPoints[ (i + 1) % midPoints.length ];
				var prevMidPoint:Number = midPoints[ i != 0 ? i - 1 : midPoints.length - 1 ];

				var prevVec:Vector2 = TEMP_VEC1.setToHeading(prevMidPoint);
				var nextVec:Vector2 = TEMP_VEC2.setToHeading(nextMidPoint);
				var midVec:Vector2 = TEMP_VEC3.setToHeading(midPoint);

				startPoints[i] = midPoint - (Vector2.angleBetween(midVec, prevVec)/2);	
				endPoints[i] = midPoint + (Vector2.angleBetween(midVec, nextVec)/2);

				i++;
			}
			
			
			i = 0;
			for each(var midPoint:Number in midPoints){
				var nextMidPoint:Number = midPoints[ (i + 1) % midPoints.length ];
				var prevMidPoint:Number = midPoints[ i != 0 ? i - 1 : midPoints.length - 1 ];

				var startVec:Vector2 = TEMP_VEC1.setToHeading(startPoints[i]);
				var endVec:Vector2 = TEMP_VEC2.setToHeading(endPoints[i]);
				
				midPoints[i] = startPoints[i] + (Vector2.angleBetween(startVec, endVec)/2);
				
				i++;
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC1);									// RELASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC2);									// RELASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC3);									// RELASE VECTOR2
		}
	
		
	}
	
}
