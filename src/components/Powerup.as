﻿package components {
	
	import framework.ecs.IComponent;
	import entities.Rocket;
	
	
	public class Powerup implements IComponent{

		public var rocket:Rocket;
		private var _enabled:Boolean = false;
		
		
		public function Powerup(rocket:Rocket) {
			this.rocket = rocket;
		}
		
		
		public function get enabled(){
			return _enabled;
		}
		
		
		public function enable(){
			_enabled = true;
			onEnabled();
		}
		
		
		public function disable(){
			_enabled = false;
			onDisabled();
		}
		
		
		protected function onEnabled(){}
		protected function onDisabled(){}

	}
	
}
