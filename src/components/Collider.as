﻿package components {
	
	import framework.ecs.IComponent;
	import framework.ecs.Entity;
	import flash.utils.Dictionary;
	
	
	public class Collider implements IComponent{

		public var radius:Number;
		public var entity:Entity;
		public var handler:Function;
		
		
		public function Collider(entity:Entity, radius:Number, handler:Function) {
			this.radius = radius;
			this.entity = entity;
			this.handler = handler;
		}


		
		public function overlaps(other:Collider):Boolean{
			var dist:Number = other.entity.position.dist(entity.position);
			return dist <= radius + other.radius;
		}
		
		
	}

	
}




