﻿package components {
	
	import framework.ecs.IComponent;
	import framework.ecs.Entity;
	import math.TargetPathSegment;
	import aspire.geom.Vector2;
	import math.PathSegment;
	import utils.TargetPathSegmentPool;
	import math.ArcPathSegment;
	import utils.ArcPathSegmentPool;

	
	public class Orbit implements IComponent{
		
		public var entity:Entity;
		public var velocity:Number = 1;
		public var distanceTravelled:Number = 0;
		
		public var path:PathSegment;
		
		
		public function Orbit(entity:Entity) {
			this.entity = entity;
		}
		
		
		public function setAroundTarget(target:OrbitTarget){
			var startAngle:Number = target.entity.position.angleToPosition(entity.position);
			path = TargetPathSegmentPool.__GET__(target, startAngle, 1, 100);
			path.segmentStartDistance = 0;
		}

		

		public function get currentPathSegment():OrbitTarget{
			return path.at(distanceTravelled);
		}
		
		
		
		public function target():OrbitTarget{
			var currentPathSegment:PathSegment;
			if(currentPathSegment is TargetPathSegment){
				return (currentPathSegment as TargetPathSegment).target;
			}
			return null;
		}

	}
	
}
