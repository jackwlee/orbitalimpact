﻿package components {
	
	import framework.ecs.IComponent;
	import framework.ecs.Entity;
	import math.PathSegment;
	import math.ArcPathSegment;
	import utils.PathSegmentPool;
	import aspire.geom.Vector2;

	
	public class PathFinder implements IComponent{
		
		public var entity:Entity;
		public var path:PathSegment;
		
		
		public function PathFinder(entity:Entity) {
			this.entity = entity;
		}

		/*
		private function calculatePotenctialPath(){
			if(path) PathSegmentPool.__RELEASE__(path);
			path = null;
			
			var orbit:Orbit = entity.getComponent( Orbit ) as Orbit;
			if(orbit.target == null) return;
			
			var potentialTarget:OrbitTarget = orbit.target.
		}
		*/
		
		
		public function findNextIdealTarget(){
			potentialTarget = null;
			
			var orbit:Orbit = entity.getComponent( Orbit ) as Orbit;
			var target:OrbitTarget = orbit.target;
			
			if(target == null) return;
			
			var trajectoryAngle:Number = target.entity.position.angleToPosition(orbit.entity.position) + (Math.PI/2 * direction);
			

			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2						
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			var TEMP_VEC_3:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			
			
			var angleVec:Vector2 = TEMP_VEC_1.setToHeading(trajectoryAngle);
			angleVec.normalize();
			
			var closestDot:Number = -1;
			var closestTarget:int;
			var found:Boolean = false;
			
			for(var i:int = 0; i < target.adjacent.length; i++){
				var midVec:Vector2 = TEMP_VEC_2.setToHeading(target.midPoints[i]);
				midVec.normalize();
				
				var dot:Number = midVec.dot(angleVec);
				if(dot > 0){
					if(dot > closestDot){
						closestTarget = i;
						closestDot = dot;
						found = true;
					}
				}
			}
			
			if(found){
				var startVec:Vector2 = TEMP_VEC_2.setToHeading(target.startPoints[closestTarget]);
				var endVec:Vector2 = TEMP_VEC_3.setToHeading(target.endPoints[closestTarget]);
				var nextTarget:OrbitTarget = target.adjacent[closestTarget];
				
				var startDot:Number = startVec.dot(angleVec);
				var endDot:Number = endVec.dot(angleVec);
				
				var dir:int = startDot > endDot ? 1 : -1;
				
				trace("Found!");
				
				/*
				adjacentArc = ArcNodePool.__GET__(null, orbit.distanceTravelled);		// GET ARCNODE
				
				var potencialArc:Arc = orbit.getPotencialArc(nextTarget, dir, direction, adjacentArc.arc);
				
				if(potencialArc == null){
					ArcNodePool.__RELEASE__(adjacentArc);								// RELEASE ARCNODE
					adjacentArc = null;
				}else{
					var nodeOfNextTarget:ArcNode = ArcNodePool.__GET__(nextTarget, adjacentArc.startDistance + adjacentArc.arc.length);
					adjacentArc.attach(nodeOfNextTarget);
					nodeOfNextTarget.arc.direction = potencialArc.inverted ? potencialArc.direction*-1 : potencialArc.direction;
					nodeOfNextTarget.continueOnFromPrev();
				}
				*/
		
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_3);					// RELEASE VECTOR2
			
		}
		
		/*
		var orbit:Orbit = this;
			
			if(adjacentArc){
				ArcNodePool.__RELEASE__(adjacentArc);				// RELEASE ARCNODE
				adjacentArc = null;
			}
				
			var trajectoryAngle:Number = target.entity.position.angleToPosition(orbit.entity.position) + (Math.PI/2 * direction);
			

			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2						
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			var TEMP_VEC_3:Vector2 = Vector2Pool.__GET__();						// GET VECTOR2
			
			
			var angleVec:Vector2 = TEMP_VEC_1.setToHeading(trajectoryAngle);
			angleVec.normalize();
			
			var closestDot:Number = -1;
			var closestTarget:int;
			var found:Boolean = false;
			
			for(var i:int = 0; i < target.adjacent.length; i++){
				var midVec:Vector2 = TEMP_VEC_2.setToHeading(target.midPoints[i]);
				midVec.normalize();
				
				var dot:Number = midVec.dot(angleVec);
				if(dot > 0){
					if(dot > closestDot){
						closestTarget = i;
						closestDot = dot;
						found = true;
					}
				}
			}
			
			if(found){
				var startVec:Vector2 = TEMP_VEC_2.setToHeading(target.startPoints[closestTarget]);
				var endVec:Vector2 = TEMP_VEC_3.setToHeading(target.endPoints[closestTarget]);
				var nextTarget:OrbitTarget = target.adjacent[closestTarget];
				
				var startDot:Number = startVec.dot(angleVec);
				var endDot:Number = endVec.dot(angleVec);
				
				var dir:int = startDot > endDot ? 1 : -1;
				//dir *= direction;
				
				adjacentArc = ArcNodePool.__GET__(null, orbit.distanceTravelled);		// GET ARCNODE
				
				var potencialArc:Arc = orbit.getPotencialArc(nextTarget, dir, direction, adjacentArc.arc);
				
				if(potencialArc == null){
					ArcNodePool.__RELEASE__(adjacentArc);								// RELEASE ARCNODE
					adjacentArc = null;
				}else{
					var nodeOfNextTarget:ArcNode = ArcNodePool.__GET__(nextTarget, adjacentArc.startDistance + adjacentArc.arc.length);
					adjacentArc.attach(nodeOfNextTarget);
					nodeOfNextTarget.arc.direction = potencialArc.inverted ? potencialArc.direction*-1 : potencialArc.direction;
					nodeOfNextTarget.continueOnFromPrev();
				}
		
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);					// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_3);					// RELEASE VECTOR2
		}
			*/
		
		/*
		private function getPotencialArc(potencialTarget:OrbitTarget, direction:Number, phase:Number, out:ArcPathSegment):ArcPathSegment {
			
			
			if(potencialTarget != this.target && this.target != null){
				var x1:Number = this.target.entity.position.x;
				var y1:Number = this.target.entity.position.y;
				var r1:Number = this.target.radius;
				
				var x2:Number = potencialTarget.entity.position.x;
				var y2:Number = potencialTarget.entity.position.y;
				var r2:Number = potencialTarget.radius;
				
				var x3:Number = entity.position.x;
				var y3:Number = entity.position.y;
				var r3:Number = 0.0001;
				
				var arc:Arc;
				
				if(direction * -phase < 0){ // Clockwise
					arc = apollonius( x1,  y1, r1,  x2,  y2,  r2,  x3,  y3,  r3, 1, out.reset());	// Rule 1
					if(arc) arc.inverted = false;
					if(arc == null){
						direction *= -1;
						arc = apollonius( x1,  y1,  -r1,  x2,  y2, -r2,  x3,  y3, -r3, 1, out.reset());	 // Rule 8
						if(arc) arc.inverted = false;
					}
				}else{ // Counter
					direction *= -1;
					arc = apollonius( x1,  y1,  r1,  x2,  y2, -r2,  x3,  y3,  r3, -1, out.reset());	// Rule 3
					
					if(arc)  arc.inverted = true;
					if(arc == null){
						direction *= -1;
						arc = apollonius( x1,  y1,  -r1,  x2,  y2, r2,  x3,  y3, -r3, -1, out.reset());		// Set Arc.temp2
						if(arc) arc.inverted = false;
					}
				}
				
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  -r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  -r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   +r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  -r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  -r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  +r3, 1, out.reset());	// Rule 1
				//arc = apollonius( x1,  y1,   -r1,  x2,  y2,  +r2,  x3,  y3,  -r3, 1, out.reset());	// Rule 1
				
				
				if(arc != null){
					arc.startAngle = arc.center.angleToPosition(entity.position);
					arc.direction = direction;
					arc.clipToIntersection(potencialTarget.entity.position.x, potencialTarget.entity.position.y, potencialTarget.radius+0.0001);
					return arc;
				}
			}
			
			return null;
			
		}
		*/
		
		
	}
	
}
