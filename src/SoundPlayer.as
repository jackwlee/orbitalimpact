﻿package monsterz.game {
	
	import flump.display.Movie;
	import treefortress.sound.SoundAS;
	
	
	public class SoundPlayer {

		
		public function SoundPlayer(movie:Movie) {
			movie.labelPassed.connect(onLabelPassed);
		}
		
		
		private function onLabelPassed(label:String){
			if(label.indexOf("sound_") > -1){
				var soundString:String = label.substring("sound_".length);
				SoundAS.play(soundString);
			}
		}
		

	}
	
}
