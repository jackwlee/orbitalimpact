﻿package services {
	
	import starling.animation.IAnimatable;
	
	public class Time implements IAnimatable{

		
		public var prev:Number = 0;

		public function get step():Number{
			return _now - prev;
		}

		
		public var advanceTime(time:Number):void{
			now += time;
		}
		
		
		private var _now:Number = 0;
		public function set now(value:Number){
			prev = _now;
			_now = value;
		}
		public function get now():Number{
			return _now;
		}
		
				
		////////////////////////////////////////////////
		//
		// Singelton
		//
		////////////////////////////////////////////////
		private static var _instance:Time;
		public static function get instance():Time{
			if(_instance == null) _instance = new Time();
			return _instance;
		}
		
	}
	
}
