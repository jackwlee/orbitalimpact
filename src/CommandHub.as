﻿package  {
	
	import flash.utils.Dictionary;

	
	public class CommandHub {

		public var executed:CommandHubSubscribers = new CommandHubSubscribers();
		public var completed:CommandHubSubscribers = new CommandHubSubscribers();

		
		//
		// Singleton
		//
		private static var _instance:CommandHub;

		public static function get instance():CommandHub{
			if(_instance == null) _instance = new CommandHub();
			return _instance;
		}

	}	
	
}



import flash.utils.Dictionary;
import framework.command.ICommand;
import flash.utils.getQualifiedClassName;
import flash.utils.getDefinitionByName;


class CommandHubSubscribers {

	private var lookup:Dictionary = new Dictionary();

	
	private function getSubscribers(commandClass:Class):Vector.<Function>{
		if(lookup[commandClass] == null && lookup[commandClass] == undefined){
			lookup[commandClass] = new Vector.<Function>();
		}
		return lookup[commandClass];
	}
	
	
	public function subscribe(commandClass:Class, callback:Function):void{
		var subscribers:Vector.<Function> = getSubscribers(commandClass);
		if(subscribers.indexOf(callback) != -1) throw("Already subscribing with function");
		subscribers.push(callback);
	}
	
	
	public function unsubscribe(commandClass:Class, callback:Function):void{
		var subscribers:Vector.<Function> = getSubscribers(commandClass);
		var index:int = subscribers.indexOf(callback);
		if(index != -1){
			subscribers.splice(index, 1);
		}
	}
	
	
	public function dispatch(command:ICommand):void{
		var klassName:String = getQualifiedClassName(command);
		var klass:Class = getDefinitionByName(klassName) as Class;
		var subscribers:Vector.<Function> = getSubscribers(klass);
		
		for each(var callback:Function in subscribers){
			callback(command);
		}
	}
	
		
}



