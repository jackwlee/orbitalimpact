﻿package {
	
	import starling.core.Starling;
	import starling.animation.Juggler;

	
	public class Jugglers{

		
		public var ui			:Juggler = new Juggler();
		public var game			:Juggler = new Juggler();
		
		
		public function Jugglers(){
			Starling.current.juggler.add(ui);
		}

		

		/////////////////////////////////////////////////////
		//
		//  Singleton
		//
		/////////////////////////////////////////////////////
		
		
		private static var _instance:Jugglers;
		
		
		public static function get instance():Jugglers{
			if(_instance == null){
				_instance = new Jugglers();
				
			}
			return _instance;
		}

		

	}
	
}
