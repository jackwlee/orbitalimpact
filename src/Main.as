﻿package  {
	
	import flash.display.MovieClip;
	import starling.core.Starling;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.geom.Rectangle;
	

	public class Main extends MovieClip{

		
		public function Main() {
			Globals.instance.nativeRoot = this;
			
			Starling.handleLostContext = true;
			
			var s:Starling = new Starling(StarlingRoot, stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE_EXTENDED );
			s.showStats = true;
			
			
			//s.enableErrorChecking = true;
			
			//s.stage.stageWidth = 1024/2;
			//s.stage.stageHeight = 768/2;
			s.start();
			
		}
		
	}
	
}
