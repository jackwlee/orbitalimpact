﻿package  {
	
	import flash.display.Sprite;
	import flash.display.Graphics;
	
	
	public class Debug extends flash.display.Sprite{
	
		//
		// Singleton
		//
		private static var _instance:Debug;
		public static function get instance():Debug{
			if(_instance == null) _instance = new Debug();
			return _instance;
		}
		
	}
	
}
