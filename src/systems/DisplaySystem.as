﻿package systems {
	
	import components.*;
	import framework.ecs.*;
	import starling.animation.IAnimatable;
	import entities.Collections;
	import entities.Animation;
	import flump.display.Movie;
	import framework.flump.FlumpLibs;
	import react.Connection;
	
	
	public class DisplaySystem extends System{

		
		public function DisplaySystem(){
			Collections.instance.animations.added.add(onAnimationAdded);
			Collections.instance.all.removed.add(onEntityRemoved);
		}
		
		
		private function onEntityRemoved(entity:Entity){
			entity.display.parent.removeChild(entity.display);
		}
		
		
		private function onAnimationAdded(entity:Entity){
			var animation:Animation = entity.getComponent(Animation) as Animation;
			var movie:Movie = FlumpLibs.instance.game.createMovie(animation.symbolId);
			entity.display.addChild(movie);
			Jugglers.instance.game.add(movie);
			movie.play();
			
			var connection:Connection = movie.labelPassed.connect(function(label:String){
				if(label == Movie.LAST_FRAME){
					connection.close();
					Jugglers.instance.game.remove(movie);
					World.instance.remove(entity);
				}
			});
		}

		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.all.entities){
				var position:Position = entity.getComponent( Position ) as Position;
				var display:Display = entity.getComponent( Display ) as Display;
				
				display.x = position.x;
				display.y = position.y;
			}
		}

		
	}
	
}
