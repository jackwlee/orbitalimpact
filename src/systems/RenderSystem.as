﻿package systems {
	
	import components.*;
	import framework.ecs.*;
	import entities.Collections;
	
	
	public class RenderSystem extends System{

		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.all.entities){
				entity.render();
			}
		}


	}
	
}
