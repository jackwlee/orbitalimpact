﻿package systems {
	
	import framework.ecs.EntityCollection;
	import framework.ecs.Entity;
	import starling.animation.IAnimatable;
	import flash.display.Graphics;
	import framework.ecs.System;
	import entities.Rocket;
	import components.OrbitTarget;
	import components.Orbit;
	import math.Arc;
	import aspire.geom.Vector2;
	import entities.Collections;
	import math.Circle;
	import math.PotentialArcs;

	
	public class DebugSystem extends System{

		private var collections:Collections = Collections.instance;
		public var graphics:Graphics = Debug.instance.graphics;
		
		
		
		override public function advanceTime(time:Number):void{
			collections.all			.forEach( drawRocketOrPlanet );
			collections.orbiters	.forEach( drawArcs );
		}
		
		
		/*
		private function drawArc(arc:Arc){
			if(isNaN(arc.endDistance)){
				graphics.drawCircle(arc.center.x, arc.center.y, arc.radius);
			}else{
				var startPosition:Vector2 = arc.positionForTime(arc.startDistance, Vector2.temp1); // Set temp 1
				graphics.moveTo(startPosition.x, startPosition.y);
				
				for(var i:Number = arc.startDistance; i <= arc.endDistance; i += 10){
					var pos:Vector2 = arc.positionForTime(i, Vector2.temp2); // Set temp 2
					graphics.lineTo(pos.x, pos.y);
				}
				
				graphics.endFill();
			}
		}
		
		
		
		private function drawRocketOrPlanet(entity:Entity){
			if(entity is Rocket){
				graphics.lineStyle(1, 0x990000);
				graphics.drawCircle(entity.position.x, entity.position.y, 20);
			}
			
			/*
			if(entity.hasComponent(OrbitTarget)){
				var target:OrbitTarget = entity.getComponent(OrbitTarget) as OrbitTarget;
				graphics.lineStyle(1, 0x009900, 0.4);
				graphics.drawCircle(entity.position.x, entity.position.y, target.radius);
			}
			*/
		}
		
		
		
		private function drawArcs(entity:Entity){
			var orbit:Orbit = entity.getComponent(Orbit) as Orbit;
			
			var arc1:Arc = new Arc();
			arc1.center.x = 0;
			arc1.center.y = 0;
			arc1.radius = 50;
			
			var arc2:Arc = new Arc();
			arc2.center.x = 40;
			arc2.center.y = -40;
			arc2.radius = 50;
			
			arc1.startDistance = 0
			arc1.startAngle = 0;
			//arc1.endTime = 50;
			arc1.direction = -1;
			arc1.clipToIntersection(arc2.center.x, arc2.center.y, arc2.radius);
			
			graphics.lineStyle(0.2, 0x999999, 0.5);
			graphics.drawCircle(arc1.center.x, arc1.center.y, arc1.radius);
			graphics.drawCircle(arc2.center.x, arc2.center.y, arc2.radius);
			
			graphics.lineStyle(0.2, 0x009999, 1);
			drawArc(arc1);
			
			/*
			// Draw owned arcs
			graphics.lineStyle(0.2, 0x990000, 0.1);
			var arc:Arc = orbit.headArc;
			while(arc != null){
				drawArc(arc);
				arc = arc.next;
			}
			*/
			
			// Draw orbit lines
			/*
			graphics.lineStyle(0.1, 0xffffff, 0.1);
			for each(var targetEntity:Entity in collections.targets.entities){
				var target:OrbitTarget = targetEntity.getComponent(OrbitTarget) as OrbitTarget;
				if(orbit.target != target){
					var arc1:Arc = orbit.apollonius(target.entity.position.x, target.entity.position.y, target.radius, 1, Arc.temp1); 	// Arc Temp1
					var arc2:Arc = orbit.apollonius(target.entity.position.x, target.entity.position.y, target.radius, -1, Arc.temp2); 	// Arc Temp2
					
					if(arc1) drawArc(arc1);
					if(arc2) drawArc(arc2);
				}
			}
			*/
			
			// Draw potencial arcs
			for each(var arcs:PotentialArcs in orbit.getPonentialArcs()){
				graphics.lineStyle(0.1, 0x009900, 0.5);
				if(arcs.clockwise) drawArc(arcs.clockwise);
				
				graphics.lineStyle(0.1, 0x990000, 0.5);
				if(arcs.counter) drawArc(arcs.counter);
				
				graphics.lineStyle(0.1, 0x999999, 0.5);
				graphics.drawCircle(arcs.target.entity.position.x, arcs.target.entity.position.y, arcs.target.radius);
			}
		
		}
		
		
	}
	
}
