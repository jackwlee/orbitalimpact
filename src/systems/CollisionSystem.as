﻿package systems {
	
	import framework.ecs.System;
	import framework.ecs.Entity;
	import framework.ecs.EntityCollection;
	import components.Collider;
	import entities.Collections;
	
	
	public class CollisionSystem extends System{

		
		override public function advanceTime(time:Number):void{
			var colliderEntities:Vector.<Entity> = Collections.instance.colliders.entities;
			
			var colliderCount:int = colliderEntities.length;
			
			for(var i:int = 0; i < colliderEntities.length && i >= 0; i++){
				var firstEntity:Entity = colliderEntities[i];
				var firstCollider:Collider = firstEntity.getComponent(Collider) as Collider;
				for(var j:int = i+1; j < colliderEntities.length && j >= 0; j++){
					var secondEntity:Entity = colliderEntities[j];
					var secondCollider:Collider = secondEntity.getComponent(Collider) as Collider;
					
					if(firstCollider.overlaps(secondCollider)){
						firstCollider.handler(secondEntity);
						secondCollider.handler(firstEntity);
					}
					
					var countDifference = colliderCount - colliderEntities.length;
					i -= countDifference;
					j -= countDifference;
					colliderCount = colliderEntities.length;
				}
			}
			
			
		}

	}
	
}
