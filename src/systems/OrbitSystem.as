﻿package systems {
	
	import framework.ecs.EntityCollection;
	import framework.ecs.System;
	import framework.ecs.Entity;
	import components.Orbit;
	import components.Velocity;
	import components.OrbitTarget;
	import components.Position;
	import entities.Collections;
	import entities.Bullet;
	import utils.Vector2Pool;
	import aspire.geom.Vector2;
	import math.ArcNode;
	
	
	public class OrbitSystem extends System{

		
		override public function advanceTime(time:Number):void{			
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
		
			for each(var entity:Entity in Collections.instance.orbiters.entities){
				var orbit:Orbit = entity.getComponent(Orbit) as Orbit;
				var velocity:Number = time * 60 * orbit.velocity;
				
				orbit.distanceTravelled += velocity;
					
				
				/*
				var additionalThrust:Number = orbit.decelerationDistance - (orbit.distanceTravelled - orbit.thrustedOn);
				if(additionalThrust < 0) additionalThrust = 0;
				additionalThrust = additionalThrust / orbit.decelerationDistance;
				additionalThrust = velocity * additionalThrust * orbit.thrustVelocity;
				orbit.distanceTravelled += additionalThrust;
				*/
				
				/*
				var arcNode:ArcNode = orbit.currentArcNode;
				var arcTime:Number = orbit.distanceTravelled - arcNode.startDistance;
				var finalPos:Vector2 = arcNode.arc.positionAtDistance(arcTime, TEMP_VEC_1);
				*/
				
				/*
				var finalPos:Vector2 = orbit.path.positionAtDistance(orbit.distanceTravelled, TEMP_VEC_1);
				entity.position.x = finalPos.x;
				entity.position.y = finalPos.y;
				entity.display.rotation = orbit.angle;
				*/
				
				var finalPos:Vector2 = orbit.path.positionAtDistance(orbit.distanceTravelled, TEMP_VEC_1);
				var finalAngle:Number = orbit.path.angleAtDistance(orbit.distanceTravelled);
				entity.position.x = finalPos.x;
				entity.position.y = finalPos.y;
				entity.display.rotation = finalAngle;
				
				//if(arcNode.arc.direction == 1) entity.display.rotation += Math.PI;
				
				
			}
			
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);							// RELEASE VECTOR2
		}
		
		
	}
	
}


