﻿package systems {
	
	import starling.animation.IAnimatable;
	import starling.animation.Juggler;
	import aspire.util.ClassUtil;
	import framework.ecs.System;

	
	public class Systems implements IAnimatable{
		
		private var collection:Vector.<System> = new Vector.<System>();
		
		
		public function Systems() {
			add( TimeSystem.create() );
			add( new LifeSystem() );
			add( new DraggerSystem() );
			add( new RocketSystem() );
			add( new PotentialPathSystem() );
			add( new OrbitSystem() );
			add( new CollisionSystem() );
			add( new MovementSystem() );
			add( new DisplaySystem() );
			add( DebugSystem.instance );
			add( new RenderSystem() );			
		}		
		
		
		public function add(system:System):void{
			var systemClass:Class = aspire.util.ClassUtil.getClass(system);
			if(has(systemClass)) throw new Error("Already added sytem: " + system);
			collection.push(system);
		}
		
		
		
		public function get(systemClass:Class):System{
			if(has(systemClass) == false) throw new Error("Cannot get system class that does not exist: " + system);
			for each(var system:System in collection){
				var toCheck:Class = aspire.util.ClassUtil.getClass(system);
				if(toCheck == systemClass){
					return system;
				}
			}
			throw new Error("This should not happen");
		}
		

		
		public function remove(systemClass:Class):void{
			if(has(systemClass) == false) throw new Error("Cannot remove system class that does not exist: " + system);
			for(var i:int = 0; i < collection.length; i++){
				var system:System = collection[i];
				var toCheck:Class = aspire.util.ClassUtil.getClass(system);
				if(toCheck == systemClass){
					collection.splice(i, 1);
					return;
				}
			}
			throw new Error("This should not happen");
		}

		
		
		public function has(klass:Class):Boolean{
			for each(var system:System in collection){
				var toCheck:Class = aspire.util.ClassUtil.getClass(system);
				if(toCheck == klass) return true;
			}
			return false;
		}
		

		
		public function advanceTime(time:Number):void{
			for each(var system:System in collection){
				system.advanceTime(time);
				system.debug();
			}
		}
		
		
		//
		// Singelton
		//
		private static var _instance:Systems;
		public static function get instance():Systems{
			if(_instance == null) _instance = new Systems();
			return _instance;
		}
		
	}
	
}
