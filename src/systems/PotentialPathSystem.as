﻿package systems {
	
	import framework.ecs.System;
	import framework.ecs.Entity;
	import entities.Collections;
	import components.PathFinder;
	import components.Orbit;
	
	
	public class PotentialPathSystem extends System{

		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.targets.entities){
				var target:OrbitTarget = entity.getComponent( OrbitTarget ) as OrbitTarget;
				target.calculateAnglesToAdjacent();
				calculateAdjacentArc(orbit.tailArcNode.arc.direction);
			}
			
			
			for each(var entity:Entity in Collections.instance.pathFinders.entities){
				var pathFinder:PathFinder = entity.getComponent(PathFinder) as PathFinder;
				var orbit:Orbit = entity.getComponent(Orbit) as Orbit;
				
				
			}
		}
		
	}
	
}

