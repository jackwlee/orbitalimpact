﻿package systems {
	
	import framework.ecs.EntityCollection;
	import framework.ecs.Entity;
	import starling.animation.IAnimatable;
	import flash.display.Graphics;
	import framework.ecs.System;
	import entities.Rocket;
	import components.OrbitTarget;
	import components.Orbit;
	import math.Arc;
	import aspire.geom.Vector2;
	import entities.Collections;
	import math.Circle;
	import math.PotentialArcs;
	import entities.Planet;
	import math.ArcNode;
	import flash.globalization.NumberParseResult;
	import utils.Vector2Pool;
	import utils.ArcPool;
	import components.Collider;
	import math.PathSegment;
	import math.ArcPathSegment;
	import utils.ArcPathSegmentPool;

	
	public class DebugSystem extends System{

		private var collections:Collections = Collections.instance;
		public var graphics:Graphics = Debug.instance.graphics;
		
		
		override public function advanceTime(time:Number):void{
		
			for each(var entity:Entity in collections.all.entities){
				drawRocketOrPlanet(entity);
			}
			
			
			for each(var entity:Entity in collections.targets.entities){
				var target:OrbitTarget = entity.getComponent(OrbitTarget) as OrbitTarget;
				graphics.lineStyle(0.2, 0x009900, 0.1);
			}
			
			
			for each(var entity:Entity in collections.orbiters.entities){
				var orbit:Orbit = entity.getComponent( Orbit) as Orbit;
				graphics.lineStyle(0.4, 0xff0000);
				drawPath(orbit.path);
			}
			
			/*
			for each(var entity:Entity in collections.colliders.entities){
				var collider:Collider = entity.getComponent(Collider) as Collider;
				graphics.lineStyle(0.3, 0x990000);
				graphics.drawCircle(entity.position.x, entity.position.y, collider.radius);
			}
			*/
			
			
			var path1:ArcPathSegment = ArcPathSegmentPool.__GET__(0, 0, 20, -Math.PI/2, 1, 60);
			var path2:ArcPathSegment = ArcPathSegmentPool.__GET__(50, 50, 80, 0, 1);
			//var path3:ArcPathSegment = new ArcPathSegment(new Vector2(-50, -50), 30, 0, 1);
			
			
			//path3.clipAtEndAngle(Math.PI/2);
			
			var intersection1:Vector2 = new Vector2();
			var intersection2:Vector2 = new Vector2();
			
			path1.intersectionsWithOtherArcPathSegment(path2, intersection1, intersection2);
			graphics.lineStyle(0.5, 0x009900);
			graphics.drawCircle(intersection1.x, intersection1.y, 5);
			graphics.drawCircle(intersection2.x, intersection2.y, 5);
			
			
			path1.clipToIntersectionWithOtherArcPathSegment(path2);
			
			path1.segmentStartDistance = 0;
			path1.tail.append(path2);
			//path1.tail.append(path3);
			
			var newPath:ArcPathSegment = ArcPathSegmentPool.__CLONE__(path1);
			newPath.head.clipStartDistance(offset);
			
			

			//trace(newPath.head.segmentLength, newPath.head.segmentStartDistance);
			//offset += 0.5;
			
			graphics.lineStyle(0.2, 0xff0000);
			drawPath(newPath);
			
			
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();
			var po:Vector2 = newPath.head.positionAtDistance(newPath.head.segmentStartDistance, TEMP_VEC_1);
			graphics.drawCircle(TEMP_VEC_1.x, TEMP_VEC_1.y, 5);
			Vector2Pool.__RELEASE__(TEMP_VEC_1);
			
			ArcPathSegmentPool.__RELEASE__(newPath);
			ArcPathSegmentPool.__RELEASE__(path1);
		}
		
		private var offset:Number = 0;
		
		
		
		public function drawPath(path:PathSegment){
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__(); 	// GET VECTOR2
			var totalLength = path.head.lengthGoingForward;
			
			for(var i:Number = path.head.segmentStartDistance; i < totalLength; i += 5){
				path.positionAtDistance(i, TEMP_VEC_1);
				graphics.drawCircle(TEMP_VEC_1.x, TEMP_VEC_1.y, 0.5);
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1); 				// RELEASE VECTOR2
		}
		
		
		
		public function drawAngle(angle:Number, position:Vector2, length:Number){
			var TEMP_VEC1:Vector2 = Vector2Pool.__GET__();			// GET VECTOR2
			
			var endOffset:Vector2 = TEMP_VEC1;
			endOffset.x = length;
			endOffset.y = 0;
			endOffset.angle = angle;
			
			graphics.moveTo(position.x, position.y);
			graphics.lineTo(position.x + endOffset.x, position.y + endOffset.y);
			
			Vector2Pool.__RELEASE__(TEMP_VEC1);						// RELEASE VECTOR2
		}
		
		
		
		public function drawRocketOrPlanet(entity:Entity){
			/*
			if(entity is Planet){
				var planet:Planet = entity as Planet;
				graphics.lineStyle(0.2, 0x38474D, 1);
				graphics.drawCircle(entity.position.x, entity.position.y, planet.orbitTarget.radius);
			}
			*/
			
			/*
			if(entity is Rocket){
				var rocket:Rocket = entity as Rocket;
				//graphics.lineStyle(0.3, 0x990000, 0.3);
				//graphics.drawCircle(entity.position.x, entity.position.y, 1);
				
				var orbit:Orbit = rocket.orbit;
				
				if(orbit.target){
					graphics.lineStyle(0.3, 0xff0000, 0.3);
					if(orbit.adjacentArc) drawArcNode(orbit.adjacentArc);
				}
				
			}
			*/
			
		}
		
		
		private function getArcAlpha(arc:Arc):Number{
			var s:Number = 200;
			var alpha:Number = arc.radius;
			if(alpha > s) alpha = s;
			alpha = s - alpha;
			alpha /= s;
			return alpha;
		}
		
		
		
		public function drawArcNode(arcNode:ArcNode):void{
			var TEMP_VEC:Vector2 = Vector2Pool.__GET__();							// GET VECTOR2
			var pos:Vector2;
			var tail:ArcNode = arcNode.tail;
			var len:Number = tail.startDistance;
			if(tail.arc.length >= tail.arc.circumference){
				len += tail.arc.circumference;
			}else{
				len += tail.arc.length;
			}
			
			var stepSize:Number =  (len-arcNode.startDistance)/120

			
			graphics.lineStyle(1, 0xffffff, 0.1);
			pos = arcNode.positionAtDistance(arcNode.startDistance, TEMP_VEC);
			graphics.moveTo(pos.x, pos.y);
			graphics.lineStyle(1, 0xffffff, 0.1);
			
			for(var i:Number = arcNode.startDistance; i < len; i += stepSize){
				var alphaValue:Number =  0.2 * (1-((i-arcNode.startDistance) / (len-arcNode.startDistance)));
				pos = arcNode.positionAtDistance(i, TEMP_VEC);
				graphics.lineTo(pos.x, pos.y);
				//graphics.drawCircle(pos.x, pos.y, 1);
			}
			Vector2Pool.__RELEASE__(TEMP_VEC);										// RELEASE VECTOR2
		}

		
		
		public function drawArc(arc:Arc, debug:Boolean = false):void{
			var TEMP_VEC:Vector2 = Vector2Pool.__GET__();							// GET VECTOR2

			var pos:Vector2;
			var len:Number = arc.length >= arc.circumference ? arc.circumference: arc.length;
			for(var i:Number = 0; i < len; i += 10){
				pos = arc.positionAtDistance(i, TEMP_VEC);
				graphics.drawCircle(pos.x, pos.y, 1);
			}
				
				/*
				var startPos:Vector2 = arc.positionAtDistance(0, TEMP_VEC);
				graphics.moveTo(startPos.x, startPos.y);
				
				if(debug) trace("StartPos: " + startPos.x + " " + startPos.y);
					
				var arcLen:Number = arc.length < 1000 ? arc.length : 1000;
				for(var i:Number = 10; i < arcLen; i += 10){
					var pos:Vector2 = arc.positionAtDistance(i, TEMP_VEC);	
					graphics.lineTo(pos.x, pos.y);
					
					if(debug) trace("Line: " + pos.x + " " + pos.y);
				}
				
				var endPos:Vector2 = arc.positionAtDistance(arc.length, TEMP_VEC);	
				graphics.lineTo(endPos.x, endPos.y);
				
				if(debug) trace("End: " + endPos.x + " " + endPos.y);
				*/
				
			
			Vector2Pool.__RELEASE__(TEMP_VEC);										// RELEASE VECTOR2
		}
		
		
		/*
		private function drawArcs(entity:Entity){
			var orbit:Orbit = entity.getComponent(Orbit) as Orbit;
			var node:ArcNode = orbit.headArcNode;
			
			graphics.lineStyle(1, 0x009900, 1);
			while(node){
				drawArc(node.arc);
				node = node.next;
			}
		}
		*/
		
		
		/////////////////////////////////////////////////////
		//
		// Singleton
		//
		/////////////////////////////////////////////////////
		private static var _instance:DebugSystem;
		public static function get instance():DebugSystem{
			if(_instance == null) _instance = new DebugSystem();
			return _instance;
		}
		
	}
	
}
