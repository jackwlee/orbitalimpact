﻿package systems {
	
	import framework.ecs.Entity;
	import components.Orbit;
	import framework.ecs.System;
	import entities.Collections;
	
	
	public class RocketSystem extends System{
		
		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.rockets.entities){
				var orbit:Orbit = entity.getComponent( Orbit ) as Orbit;
				
				/*
				if(orbit.target){
					orbit.target.calculateAnglesToAdjacent();
					orbit.calculateAdjacentArc(orbit.tailArcNode.arc.direction);
				}
				*/
			}
		}
		
	}
	
}
