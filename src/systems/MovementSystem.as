﻿package systems {
	
	import components.*;
	import framework.ecs.*;
	import starling.animation.IAnimatable;
	import entities.Collections;
	import aspire.geom.Vector2;
	import utils.Vector2Pool;
	
	
	public class MovementSystem extends System{

		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.all.entities){
				entity.position.x += entity.velocity.x;
				entity.position.y += entity.velocity.y;
			}

			for each(var entity:Entity in Collections.instance.attractors.entities){
				var attractor:Attractor = entity.getComponent( Attractor ) as Attractor;
				
				var xDif:Number = attractor.targetPosition.x - entity.position.x;
				var yDif:Number = attractor.targetPosition.y - entity.position.y;

				entity.position.x += xDif * 0.01;
				entity.position.y += yDif * 0.01;
				
				
				/*
				var normalized:Vector2 = Vector2Pool.__GET__();		// GET VECTOR2
				normalized.x = entity.velocity.x;
				normalized.y = entity.velocity.y;
				normalized.normalize();
				
				var angleBetween:Number = Vector2.angleBetween(normalized, attractor.headingToPosition);
				
				attractor.entity.velocity.angle += angleBetween * attractor.attraction;
				
				Vector2Pool.__RELEASE__(normalized);				// RELEASE VECTOR2
				*/
			}
			
		}
		
	}
	
}
