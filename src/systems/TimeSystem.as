﻿package systems {
	
	import starling.animation.IAnimatable;
	import monsterz.game.Service;
	import framework.ecs.System;
	
	
	public class TimeSystem extends System{

		
		public var prev:Number = 0;

		
		public function get step():Number{
			return _now - prev;
		}

		
		override public function advanceTime(time:Number):void{
			now += time;
		}
		
		
		private var _now:Number = 0;
		public function set now(value:Number){
			prev = _now;
			_now = value;
		}
		public function get now():Number{
			return _now;
		}
		
				
		////////////////////////////////////////////////
		//
		// Singelton
		//
		////////////////////////////////////////////////
		private static var _instance:TimeSystem;
		public static function get instance():TimeSystem{
			if(_instance == null) throw("Must create time system first!");
			return _instance;
		}
		
		
		public static function create():TimeSystem{
			_instance = new TimeSystem();
			return _instance;
		}
		
	}
	
}
