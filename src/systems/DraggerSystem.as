﻿package systems {
	
	import framework.ecs.System;
	import framework.ecs.Entity;
	import components.Dragger;
	import components.Collider;
	import entities.Collections;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import flash.geom.Point;

	
	public class DraggerSystem extends System{


		
		public function DraggerSystem(){
			Collections.instance.draggers.added.add(onEntityAdded);
		}
		
		
		
		private function onEntityAdded(entity:Entity){
			entity.display.addEventListener(TouchEvent.TOUCH, onTouch);
			entity.removed.addOnce(onEntityRemoved);

			var dragger:Dragger = entity.getComponent(Dragger) as Dragger;
			
			function onTouch(e:TouchEvent){
				for each(var touch:Touch in e.touches){
					switch(touch.phase){
						//
						case TouchPhase.BEGAN:
							var localPoint:Point = touch.getLocation(entity.display);
							dragger.touchX = localPoint.x;
							dragger.touchY = localPoint.y;
						break;
						//
						case TouchPhase.MOVED:
							var localPoint:Point = touch.getLocation(entity.display.parent);
							entity.position.x = localPoint.x - dragger.touchX;
							entity.position.y = localPoint.y - dragger.touchY;
						break;
					}
				}
			}
			
			function onEntityRemoved(entity:Entity){
				entity.display.removeEventListener(TouchEvent.TOUCH, onTouch);
			}
		}

		
	}
	
}
