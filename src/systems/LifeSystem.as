﻿package systems {

	import framework.ecs.Entity;
	import entities.Collections;
	import components.Life;
	import framework.ecs.System;
	
	
	public class LifeSystem extends System{

		
		override public function advanceTime(time:Number):void{
			for each(var entity:Entity in Collections.instance.lifes.entities){
				var life:Life = entity.getComponent( Life ) as Life;
				
				if(life.remaining > 0){
					life.remaining--;
				}
				
				if(life.remaining <= 0){
					life.remaining = 0;
					if(life.onComplete){
						life.onComplete(entity);
					}
				}
			}
		}
		
		
	}
	
}
