﻿package  {
	
	import starling.display.Sprite;
	import framework.ui.*;
	import framework.flump.*;
	import framework.loaders.ScreensLibLoader;
	import framework.command.*;
	import starling.display.DisplayObject;
	import starling.core.Starling;
	
	
	public class StarlingRoot extends Sprite{

		
		public function StarlingRoot() {
			super();
			Globals.instance.root = this;
			this.scaleX = Globals.instance.scaleFactor;
			this.scaleY = Globals.instance.scaleFactor;
			
			trace("Scale factor: " + Globals.instance.scaleFactor);
			
			Debug.instance.scaleX = scale;
			Debug.instance.scaleY = scale;
			Debug.instance.x = Globals.instance.nativeRoot.stage.stageWidth/2;
			Debug.instance.y = Globals.instance.nativeRoot.stage.stageHeight/2;
			Globals.instance.nativeRoot.addChild(Debug.instance);
			
			new ScreensLibLoader().execute(new CallbackWhenComplete(function(){
				var background:DisplayObject = FlumpLibs.instance.screens.createDisplayObject("ScreensBackground");
				addChild(background);
				
				var starField:StarField = new StarField();
				addChild(starField);
				
				var screenManager:ScreenManager = ScreenManager.instance;
				screenManager.x = 1024/4;
				screenManager.y = 768/4;
			
				addChild(screenManager);
				//screenManager.change(new ScreenMain());
				screenManager.change(new ScreenGame());
			}));
			
		}

	}
	
}
