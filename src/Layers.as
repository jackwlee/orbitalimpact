﻿package  {
	
	import starling.display.Sprite;

	
	public class Layers extends Sprite{
		
		
		public var bullets		:Sprite = create();
		public var rockets		:Sprite = create();
		public var planet		:Sprite = create();
		public var itemBoxes	:Sprite = create();
		public var explosions	:Sprite = create();
		

		private var pendingToAdd:Vector.<Sprite>;
		
		
		public function Layers(){
			super();
			while(pendingToAdd.length > 0) addChild(pendingToAdd.shift());
		}
		
		
		private function create():Sprite{
			if(pendingToAdd == null) pendingToAdd = new Vector.<Sprite>();
			var sprite:Sprite = new Sprite();
			pendingToAdd.push(sprite);
			return sprite;
		}
		
		
		//
		// Singleton
		//
		private static var _instance:Layers;
		public static function get instance():Layers{
			if(_instance == null) _instance = new Layers();
			return _instance;
		}

	}
	
}
