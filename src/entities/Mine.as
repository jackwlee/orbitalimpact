﻿package entities {
	
	import framework.ecs.Entity;
	import components.Orbit;
	import components.Collider;
	import framework.ecs.World;
	import components.Life;
	import flump.display.Movie;

	
	public class Mine extends Entity{

		public var orbit:Orbit;
		public var collider:Collider;
		public var life:Life;
		
		
		public function Mine() {
			orbit = new Orbit(this);
			collider = new Collider(this, 30, onCollision);
			life = new Life(60, onLifeExpired);
			
			orbit.velocity *= 1.25;
			orbit.thrustVelocity *= 1.8;
			orbit.decelerationDistance *= 1.2;
			
			var mineMovie:Movie = display.addMovie("Mine");
			Layers.instance.bullets.addChild(mineMovie);
			mineMovie.loop();
			
			World.instance.add(this);
		}
		
			
		private function explode(){
			
		}
		
		
		private function onLifeExpired(entity:Entity){
			trace("Time elapsed");
			//explode();
		}
		
		
		public function onCollision(other:Entity){
			//explode();
		}
		
	
	}
	
}
