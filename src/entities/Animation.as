﻿package entities {

	import framework.ecs.World;
	import framework.ecs.Entity;
	import starling.display.Sprite;
	import aspire.geom.Vector2;
	
	
	public class Animation extends Entity{

		public var symbolId:String;
		public var playOnce:Boolean;
		
		
		public function Animation(symbolId:String, parent:Sprite, position:Vector2, playOnce:Boolean = true) {
			this.symbolId = symbolId;
			this.playOnce = playOnce;
			parent.addChild(display);
			
			this.position.x = position.x;
			this.position.y = position.y;
			
			World.instance.add(this);			
		}
		
		
	}
	
}
