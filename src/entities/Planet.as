﻿package entities {
	
	import framework.ecs.Entity;
	import components.Position;
	import components.OrbitTarget;
	import framework.ecs.World;
	import starling.display.Quad;
	import starling.display.DisplayObject;
	import framework.flump.FlumpLibs;
	import flump.display.Movie;

	
	public class Planet extends Entity{
		
		public var orbitTarget:OrbitTarget;

		
		public function Planet(){
			orbitTarget = new OrbitTarget(this);
			orbitTarget.radius = (Math.random() * 30) + 30;
			
			var planet:Movie = FlumpLibs.instance.game.createMovie("Planet");
			planet.goTo(Math.floor(Math.random() * planet.numFrames));
			display.addChild(planet);
			
			planet.width = (orbitTarget.radius * 2) - 30;
			planet.height = (orbitTarget.radius * 2) - 30;
			
			Layers.instance.planet.addChild(display);			
			World.instance.add(this);
		}

	}
	
}
