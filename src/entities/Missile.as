﻿package entities {
	
	import framework.ecs.World;
	import components.Velocity;
	import aspire.geom.Vector2;
	import components.Orbit;
	import components.OrbitTarget;
	import math.Arc;
	import framework.ecs.Entity;
	import components.Collider;
	import components.Attractor;
	
	
	public class Missile extends Entity{

		public var collider:Collider;
		public var attractor:Attractor;
		
		
		public function Missile(owner:Rocket, target:OrbitTarget) {
			collider = new Collider(this, 3, onCollision);
			attractor = new Attractor(this, 0.1, target.entity.position);
			
			position.x = owner.position.x;
			position.y = owner.position.y;
			
			display.addImage("Missile");
			
			Layers.instance.bullets.addChild(display);
			World.instance.add(this);
			
			velocity.x = 0;
			velocity.y = 0.3;
			//velocity.angle = owner.angle;
		}
		
		
		private function onCollision(other:Entity){
			
		}

		
	}

	
}

