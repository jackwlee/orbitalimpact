﻿package entities {
	
	import flash.geom.Point;
	import framework.ecs.*;
	import components.*;
	import starling.display.Quad;
	import starling.core.Starling;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import framework.ui.ScreenManager;
	import aspire.geom.Vector2;
	import starling.events.KeyboardEvent;
	import math.Arc;
	import math.ArcNode;
	import utils.Vector2Pool;
	import starling.display.DisplayObject;
	import framework.flump.FlumpLibs;

	
	public class Rocket extends Entity{

		public var orbit:Orbit;
		public var collider:Collider;
		public var shield:Shield;
		public var pathFinder:PathFinder;
		
		
		
		public function Rocket() {
			orbit = new Orbit(this);
			collider = new Collider(this, 7, onCollision);
			shield = new Shield(this);
			pathFinder = new PathFinder(this);
			
			var rocket:DisplayObject = framework.flump.FlumpLibs.instance.game.createDisplayObject("Rocket");
			display.addChild(rocket);
			
			Layers.instance.rockets.addChild(display);
			World.instance.add(this);
			
			orbit.velocity = 1;
		}
		
		
		public function onCollision(other:Entity){
			trace("Item collided with: " + other);
		}

		/*
		public function fireBullet(){
			var heading:ArcNode = orbit.adjacentArc ? orbit.adjacentArc.clone() : orbit.currentArcNode.clone();
			
			var bullet:Bullet = new Bullet();
			var path:ArcNode = heading;
			bullet.orbit.setArcNode(path);
			bullet.orbit.createdOn = orbit.createdOn + orbit.distanceTravelled;
			
			bullet.orbit.headArcNode.positionAtDistance(bullet.orbit.distanceTravelled, bullet.position);
		}
		
		
		public function fireMissile(){
			var heading:ArcNode = orbit.adjacentArc ? orbit.adjacentArc.clone() : orbit.currentArcNode.clone();

			while(heading.target == null && heading != null) heading = heading.next;
			
			if(heading){
				var missile:Missile = new Missile(this, heading.target);
			}
		}
		
		
		
		public function plantMine(){
			var heading:ArcNode = orbit.currentArcNode.clone();
			
			var mine:Mine = new Mine();
			var path:ArcNode = heading;
			mine.orbit.setArcNode(path);
			mine.orbit.createdOn = orbit.distanceTravelled + 5;
			//mine.orbit.headArcNode.pos
		}
		
		
		override public function render(){
			var arcNode:ArcNode = orbit.currentArcNode;

			var arcTime:Number = orbit.distanceTravelled - arcNode.startDistance;
			
			var TEMP_VEC1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			var finalPos:Vector2 = arcNode.arc.positionAtDistance(arcTime, TEMP_VEC1);
			position.x = finalPos.x;
			position.y = finalPos.y;
			display.rotation = orbit.angle;
			if(arcNode.arc.direction == 1) display.rotation += Math.PI;
			//if(arcNode.arc.inverted == false) display.rotation += Math.PI;
			
			Vector2Pool.__RELEASE__(TEMP_VEC1);							// RELEASE VECTOR2
			
			shield.render();
		}
		*/
		

		
	}
	
}


