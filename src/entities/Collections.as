﻿package entities {
	
	import components.*;
	import framework.ecs.EntityCollection;
	import framework.ecs.Entity;
	
	
	public class Collections {

		
		public var all			:EntityCollection = create([ ]);
		public var orbiters		:EntityCollection = create([ Orbit ]);
		public var targets		:EntityCollection = create([ OrbitTarget ]);
		public var colliders	:EntityCollection = create([ Collider ]);
		public var draggers		:EntityCollection = create([ Dragger ]);
		public var animations	:EntityCollection = create([ Animation ]);
		public var attractors	:EntityCollection = create([ Attractor ]);
		public var lifes		:EntityCollection = create([ Life ]);
		public var rockets		:EntityCollection = create([ Rocket ]);
		public var pathFinders	:EntityCollection = create([ PathFinder ]);
		
		
		
		private function create(types:Array):EntityCollection{
			return new EntityCollection(types);
		}
		
		
		//
		// Singleton
		//
		private static var _instance:Collections;
		public static function get instance():Collections{
			if(_instance == null) _instance = new Collections();
			return _instance;
		}

		
	}

	
}
