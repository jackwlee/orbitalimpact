﻿package entities {

	import framework.ecs.Entity;
	import components.*;
	import framework.flump.FlumpLibs;
	import framework.ecs.World;
	
	
	public class ItemBox extends Entity {

		public var collider:Collider;
		public var dragger:Dragger;
		

		public function ItemBox() {
			collider = new Collider(this, 7, onCollision);
			dragger = new Dragger();
			
			display.addMovie("Box");
			
			Layers.instance.itemBoxes.addChild(display);
			World.instance.add(this);
		}
		
		
		
		private function onCollision(other:Entity){
			if(other is Rocket){
				World.instance.remove(this);
				
				var rocket:Rocket = other as Rocket;
				rocket.shield.enable();
				
				//new Animation("LittleExplosion", Layers.instance.explosions, position);
			}
		}
		
		
	}
	
}
