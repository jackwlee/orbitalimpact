﻿package entities {
	
	import flash.geom.Point;
	import framework.ecs.*;
	import components.*;
	import starling.display.Quad;
	import starling.core.Starling;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import framework.ui.ScreenManager;
	import aspire.geom.Vector2;
	import starling.events.KeyboardEvent;
	import math.Arc;
	import math.ArcNode;
	import utils.Vector2Pool;
	import flash.html.script.Package;

	
	public class Bullet extends Entity{

		public var orbit:Orbit;
		public var collider:Collider;
		
		
		
		public function Bullet() {
			orbit = new Orbit(this);
			collider = new Collider(this, 3, onCollision);
			
			orbit.velocity *= 1.25;
			orbit.thrustVelocity *= 1.8;
			orbit.decelerationDistance *= 1.2;
			
			display.addImage("Bullet");
			
			Layers.instance.bullets.addChild(display);
			
			World.instance.add(this);
		}
		
		
		
		public function onCollision(other:Entity){
			if(other is Bullet){
				if(World.instance.isAdded(other)){
					var animationPosition:Vector2 = Vector2.lerp(position, other.position, 0.5);
					new Animation("LittleExplosion", Layers.instance.explosions, position, animationPosition);
				}
				World.instance.remove(this);
			}
			
			if(other is Rocket){
				var rocket:Rocket = other as Rocket;
				if(rocket.shield.enabled == false){
					new Animation("Explosion1", Layers.instance.explosions, rocket.position);
					World.instance.remove(rocket);
				}else{
					var bulletAnimation:Animation = new Animation("LittleExplosion", Layers.instance.explosions, position);
					bulletAnimation.display.alpha = 0.5;
					rocket.shield.disable();
				}
				World.instance.remove(this);
			}
		}
		
		
		public function get angle():Number{
			var arcNode:ArcNode = orbit.currentArcNode;
			return arcNode.arc.angleForDistance(orbit.distanceTravelled + arcNode.startDistance);
		}

		
		
		
		override public function render(){
			
			//var arcNode:ArcNode = orbit.currentArcNode;

			var arcTime:Number = orbit.distanceTravelled;
			//var arcTime:Number = arcNode.startDistance;
			
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var finalPos:Vector2 = orbit.headArcNode.positionAtDistance(orbit.createdOn + orbit.distanceTravelled, TEMP_VEC_1);
			
			//var finalPos:Vector2 = arcNode.arc.positionAtDistance(arcTime, TEMP_VEC_1);
			position.x = finalPos.x;
			position.y = finalPos.y;
			//display.rotation = arcNode.arc.angleForDistance(arcTime);
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
		
			//display.rotation = angle;
			//display.scaleX = 3 + (Math.PI/2);
		}
	

		
	}
	
}
