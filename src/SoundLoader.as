﻿package monsterz.game {
	
	import flash.events.Event; 
	import flash.media.Sound; 
	import flash.net.URLRequest; 
	import treefortress.sound.SoundAS;
	
	public class SoundLoader implements ICommand{

		private var delegate:CommandDelegate;
		private var soundId:String;
		private var path:String;

		
		public function SoundLoader(delegate:CommandDelegate, soundId:String, path:String) {
			this.delegate = delegate;
			this.soundId = soundId;
			this.path = path;
		}

		
		public function execute(){
			trace("Loading: " + path);
			var sound:Sound = new Sound(); 
			sound.addEventListener(Event.COMPLETE, onSoundLoaded); 
			var req:URLRequest = new URLRequest(path); 
			sound.load(req);
		}
		
		
		private function onSoundLoaded(event:Event):void{ 
			SoundAS.addSound(soundId, event.target as Sound);
			delegate.completed(this);
		}

	}
	
}




 

 
