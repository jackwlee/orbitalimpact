﻿package utils {

	import math.Arc;
	import math.ArcPathSegment;
	import math.PathSegment;
	
	
	public class ArcPathSegmentPool extends ObjectPool{

		private static var instance:ArcPathSegmentPool = new ArcPathSegmentPool();


		
		public static function __GET__(centerX:Number, centerY:Number, radius:Number, startAngle:Number, direction:Number, segmentLength:Number = -1):ArcPathSegment{
			var arcPathSegment:ArcPathSegment = instance.__GET__() as ArcPathSegment;
			arcPathSegment.center.x = centerX;
			arcPathSegment.center.y = centerY;
			arcPathSegment.radius = radius;
			arcPathSegment.startAngle = startAngle;
			arcPathSegment.direction = direction;
			arcPathSegment.segmentLength = segmentLength;
			
			if(arcPathSegment.segmentLength == -1) arcPathSegment.segmentLength = arcPathSegment.circumference;
			
			arcPathSegment.segmentStartDistance = 0;
			
			arcPathSegment.next = null;
			arcPathSegment.prev = null;
			
			return arcPathSegment;
		}
		
		
		public static function __CLONE__(arcPathSegment:ArcPathSegment):ArcPathSegment{
			var cloned:ArcPathSegment = ArcPathSegmentPool.__GET__(
				arcPathSegment.center.x,
				arcPathSegment.center.y,
				arcPathSegment.radius,
				arcPathSegment.startAngle,
				arcPathSegment.direction,
				arcPathSegment.segmentLength
			);	
			
			cloned.segmentStartDistance = arcPathSegment.segmentStartDistance;
			if(arcPathSegment.next) cloned.next = PathSegmentPool.__CLONE__(arcPathSegment.next);
			return cloned;
		}

		
		
		public static function __RELEASE__(arcPathSegment:ArcPathSegment):void{
			var next:PathSegment = arcPathSegment.next;
			instance.__RELEASE__(arcPathSegment);
			if(next) PathSegmentPool.__RELEASE__(next);
		}
		
		
		override public function createForPool():*{
			return new ArcPathSegment();
		}
		
		

	}

	
}
