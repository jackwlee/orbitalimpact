﻿package utils {

	import math.Arc;
	import math.TargetPathSegment;
	import components.OrbitTarget;
	import math.PathSegment;
	
	
	public class TargetPathSegmentPool extends ObjectPool{

		private static var instance:TargetPathSegmentPool = new TargetPathSegmentPool();


		public static function __GET__(target:OrbitTarget, startAngle:Number, direction:Number, segmentLength:Number = -1):TargetPathSegment{
			var targetPathSegment:TargetPathSegment = instance.__GET__() as TargetPathSegment;
			targetPathSegment.target = target;
			targetPathSegment.center.x = target.entity.position.x;
			targetPathSegment.center.y = target.entity.position.y;
			targetPathSegment.radius = target.radius;
			targetPathSegment.startAngle = startAngle;
			targetPathSegment.direction = direction;
			
			if(segmentLength == -1) targetPathSegment.segmentLength = targetPathSegment.circumference;
			else targetPathSegment.segmentLength = segmentLength;
			
			targetPathSegment.next = null;
			targetPathSegment.prev = null;
			
			return targetPathSegment;
		}

		
		public static function __CLONE__(targetPathSegment:TargetPathSegment):TargetPathSegment{
			var cloned:TargetPathSegment = TargetPathSegmentPool.__GET__(
				targetPathSegment.target,
				targetPathSegment.startAngle,
				targetPathSegment.direction,
				targetPathSegment.segmentLength
			);	
			
			cloned.segmentStartDistance = targetPathSegment.segmentStartDistance;
			
			if(targetPathSegment.next) cloned.next = PathSegmentPool.__CLONE__(targetPathSegment.next);
			return cloned;
		}

		
		
		public static function __RELEASE__(targetPathSegment:TargetPathSegment):void{
			var next:PathSegment = targetPathSegment.next;
			instance.__RELEASE__(targetPathSegment);
			if(next) PathSegmentPool.__RELEASE__(next);
		}
		
		
		
		override public function createForPool():*{
			return new TargetPathSegment();
		}
		
		

	}

	
}
