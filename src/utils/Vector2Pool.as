﻿package utils {

	import aspire.geom.Vector2;
	
	
	public class Vector2Pool extends ObjectPool{

		private static var instance:Vector2Pool = new Vector2Pool();


		public static function __GET__():Vector2{
			return instance.__GET__() as Vector2;
		}

		
		
		public static function __RELEASE__(vec:Vector2):void{
			instance.__RELEASE__(vec);
		}
		
		
		override public function createForPool():*{
			return new Vector2();
		}
		
		
		override public function initObject(object:*):void{
			var vec:Vector2 = object as Vector2;
			vec.x = 0;
			vec.y = 0;
		}
		

	}
	
}
