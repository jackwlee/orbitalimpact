﻿package utils {

	import components.OrbitTarget;
	import math.Arc;
	import math.ArcNode;
	
	
	public class ArcNodePool extends ObjectPool{

		private static var instance:ArcNodePool = new ArcNodePool();


		public static function __GET__(target:OrbitTarget, startDistance:Number):ArcNode{
			var arcNode:ArcNode =  instance.__GET__() as ArcNode;
			arcNode.target = target;
			arcNode.startDistance = startDistance;
			if(target){
				arcNode.arc.radius = target.radius;
				arcNode.arc.center.x = target.entity.position.x;
				arcNode.arc.center.y = target.entity.position.y;
			}
			return arcNode;
		}

		
		
		public static function __RELEASE__(arcNode:ArcNode):void{
			instance.__RELEASE__(arcNode);
		}
		
		
		public static function __TAKE__(arcNode:ArcNode):void{
			instance.take(arcNode);
			if(arcNode.next) instance.take(arcNode.next);
		}
		
		
		override public function createForPool():*{
			return new ArcNode(new Arc(), null, 0);
		}
		
		
		override public function initObject(object:*):void{
			var arcNode:ArcNode = object as ArcNode;
			arcNode.next = null;
			arcNode.prev = null;
		}
		
		
		override public function release(object:*):void{
			var arcNode:ArcNode = object as ArcNode;
			if(arcNode.next) __RELEASE__(arcNode.next);
		}
		

	}
	
}
