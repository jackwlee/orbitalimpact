﻿package utils {

	import math.Arc;
	import math.ArcPathSegment;
	import math.TargetPathSegment;
	import math.PathSegment;
	
	
	public class PathSegmentPool extends ObjectPool{

		private static var instance:PathSegmentPool = new PathSegmentPool();

		
		public static function __CLONE__(pathSegment:PathSegment):PathSegment{
			if(pathSegment is ArcPathSegment) return ArcPathSegmentPool.__CLONE__(pathSegment as ArcPathSegment);
			if(pathSegment is TargetPathSegment) return TargetPathSegmentPool.__CLONE__(pathSegment as TargetPathSegment);
			
			return null;
		}
		
		
		
		public static function __RELEASE__(pathSegment:PathSegment):void{
			if(pathSegment is ArcPathSegment) return ArcPathSegmentPool.__RELEASE__(pathSegment as ArcPathSegment);
			if(pathSegment is TargetPathSegment) return TargetPathSegmentPool.__RELEASE__(pathSegment as TargetPathSegment);
		}
		
		
	}
	
}