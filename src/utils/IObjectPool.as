﻿package utils {
	
	interface IObjectPool {

		public function createForPool():*;
		public function initObject(object:*):void;
		public function take(object:*):void;
		
	}
	
}
