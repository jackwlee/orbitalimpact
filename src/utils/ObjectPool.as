﻿package utils {
	
	import flash.utils.Dictionary;

	
	public class ObjectPool {

		protected var count:int = 0;
		protected var used:Dictionary = new Dictionary();
		protected var available:Array = [];
		
		
		
		public function createForPool():*{
		}
		
		
		public function initObject(object:*):void{
		}
		
		
		public function release(object:*):void{
		}
	
		
		public function take(object:*):void{
			if(used[object] == null || used[object] == undefined) throw("Cannot take object: " + object + "!");
			count--;
			delete used[object];
		}
		
		
		public function __GET__():*{
			var newObject:*;
			if(available.length == 0){
				count++;
				newObject = createForPool();
				trace("Created " + newObject + " " + count);
			}else{
				newObject = available.pop();
			}
			used[newObject] = newObject;
			initObject(newObject);
			return newObject;
		}
		
		
		public function __RELEASE__(object:*):void{
			if(used[object] == null || used[object] == undefined) throw("Releasing: " + object + " that cannot be released!");
			release(object);
			used[object] = null;
			available[available.length] = object;
		}

	}
	
}
