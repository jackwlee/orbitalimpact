﻿package utils {

	import math.Arc;
	
	
	public class ArcPool extends ObjectPool{

		private static var instance:ArcPool = new ArcPool();


		public static function __GET__():Arc{
			return instance.__GET__() as Arc;
			
		}

		
		public static function __RELEASE__(arc:Arc):void{
			instance.__RELEASE__(arc);
		}
		
		
		override public function createForPool():*{
			return new Arc();
		}
		
		
		override public function initObject(object:*):void{
			var arc:Arc = object as Arc;
			arc.inverted = false;
		}
		

	}
	
}
