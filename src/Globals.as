﻿package {

	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flump.display.Library;
	import starling.display.Sprite;
	import monsterz.game.GameBaseStarling;
	import monsterz.game.GameInit;
	import starling.animation.Juggler;
	import starling.core.Starling;
	import flash.display.DisplayObjectContainer;
	
	
	public class Globals {

		private static var _instance:Globals;
		
		public var nativeRoot:DisplayObjectContainer;
		public var root:Sprite;
		public var ui:Sprite;
		public var game:Sprite;
		
		
		public function get resolution():Number{
			return Math.ceil(scaleFactor);
		}
		

		public function get scaleFactor():Number{
			return (Starling.context.backBufferHeight/768) * 2;
		}
		
		
		public function get gameHeight():Number{
			return root.stage.stageHeight / scaleFactor;
		}

		
		public function get gameWidth():Number{
			return root.stage.stageWidth / scaleFactor;
		}
		
		
		public static function get instance():Globals{
			if(_instance == null) _instance = new Globals();
			return _instance;
		}
		
		
	}
	
}
