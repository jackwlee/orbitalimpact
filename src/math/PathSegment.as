﻿package math{
	
	import aspire.geom.Vector2;

	
	public class PathSegment {

		public var segmentStartDistance:Number;
		public var segmentLength:Number;
		
		public var next:PathSegment;
		public var prev:PathSegment;
		
		
		
		public function append(other:PathSegment):void{
			if(next) next.prev = null;
				
			next = other;
			other.prev = this;
			
			var path:PathSegment = other;
			while(path){
				path.segmentStartDistance = path.prev.segmentStartDistance + path.prev.segmentLength;
				path = path.next;
			}
		}
		

		public function at(globalDistance:Number):PathSegment{
			if(distanceFallsOnPath(globalDistance)) return this;
			else if(next) return next.at(globalDistance);
			else return null;
		}
		
		
		public function resetDistance(){
			if(this != head) throw(new Error("Can only perform reset distance on path head!"));
			offsetStartDistance(-segmentStartDistance);
		}
		

		public function clipStartDistance(distanceOffset:Number):void{
			throw("Must override offset start");
		}
		
		
		protected function offsetStartDistance(by:Number){
			segmentStartDistance += by;
			if(next) next.offsetStartDistance(by);
		}
			

		public function get tail():PathSegment{
			if(next == null) return this;
			else return next.tail;
		}
		

		
		public function get head():PathSegment{
			if(prev == null) return this;
			else return prev.head;
		}
		
		
		
		public function get segmentEndDistance():Number{
			return segmentStartDistance + segmentLength;
		}
		
		
			
		public function distanceFallsOnPath(globalDistance:Number):Boolean{
			return globalDistance >= segmentStartDistance && globalDistance < segmentEndDistance;
		}
		
		
		
		public function get lengthGoingForward():Number{
			if(next){
				return segmentLength + next.lengthGoingForward;
			}else{
				return segmentLength;
			}
		}
		
		
		
		public function positionAtDistance(globalDistance:Number, out:Vector2):Vector2{
			throw new Error("Must override positionAtDistance for: " + this);
		}

	
		
		public function angleAtDistance(globalDistance:Number):Number{
			throw new Error("Must override angleAtDistance for: " + this);
		}
		
		
	}

	
}
