﻿package math {
	
	import aspire.geom.Vector2;
	import components.OrbitTarget;
	import utils.Vector2Pool;
	import utils.ArcPool;

	
	public class Arc{
		
		
		public const center:Vector2 = new Vector2();
		public var radius:Number;
		public var startAngle:Number = 0;
		public var direction:Number = 1; 				// -1 or 1
		public var inverted:Boolean = false;
	
		
		private var _length:Number = 0xffffff;
		public function get length():Number{
			return _length;
		}
		public function set length(value:Number):void{
			if(value < 0) value *= -1;
			_length = value;
		}
		

		
		public function reset():Arc{
			radius = 0;
			length = 0xffffff;
			startAngle = 0;
			center.x = 0;
			center.y = 0;
			
			return this;
		}
		
		
		public function clone():Arc{
			var arc:Arc = new Arc();
			arc.radius = radius;
			arc.length = length;
			arc.startAngle = startAngle;
			arc.center.x = center.x;
			arc.center.y = center.y;
			arc.direction = direction;
			return arc;
		}
		
		
		public function cloneInto(arc:Arc):void{
			arc.radius = radius;
			arc.length = length;
			arc.startAngle = startAngle;
			arc.center.x = center.x;
			arc.center.y = center.y;
			arc.direction = direction;
		}
		
		
		
		[inline] public function positionAtDistance(distance:Number, out:Vector2):Vector2{
			out.x = 0;
			out.y = radius;
			out.angle = angleForDistance(distance);
			out.add(center);
			return out;
		}
		
		
		
		public function intersectionsWithCircle(other:Arc, out1:Vector2, out2:Vector2) {
			var    x1 = center.x; // center of first circle
			var    y1 = center.y;
			var    d1 = radius; // radius
			
			var    x2 = other.center.x; // center of second circle
			var    y2 = other.center.y;
			var    d2 = other.radius;  // radius

			// calculate the intersections 
			var a = d2;
			var b = d1;
			var c = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
			var d = (b*b+c*c-a*a)/(2*c);
			var h = Math.sqrt(b*b-d*d);
			
			out1.x = (x2-x1)*d/c + (y2-y1)*h/c +  x1;
			out1.y = (y2-y1)*d/c - (x2-x1)*h/c +  y1;
			out2.x = (x2-x1)*d/c - (y2-y1)*h/c +  x1;
			out2.y = (y2-y1)*d/c + (x2-x1)*h/c +  y1;
		}

		
		
		public function get circumference():Number{
			return Math.PI * radius * 2;
		}
		

		// http://stackoverflow.com/questions/14066933/direct-way-of-computing-clockwise-angle-between-2-vectors
		public function clipAtEndAngle(angle:Number):void{

			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			var a1:Vector2 = TEMP_VEC_1.setToHeading(startAngle);
			var a2:Vector2 = TEMP_VEC_2.setToHeading(angle);

			var angleBetween:Number;
			
			if(direction > 0){
				angleBetween = Math.atan2(a1.cross(a2), a1.dot(a2));
			}else{
				angleBetween = Math.atan2(a2.cross(a1), a2.dot(a1));
			}
			
			if(angleBetween >= 0){
				length = angleBetween * radius;
			}else{
				length = (Math.PI * radius * 2) + (angleBetween * radius);
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);						// RELEASE VECTOR2
		}
		
		
		public function clipToIntersection(x:Number, y:Number, radius:Number):void{
			var TEMP_ARC_1:Arc = ArcPool.__GET__();						// GET ARC
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			var circle:Arc = TEMP_ARC_1;
			circle.center.x = x;
			circle.center.y = y;
			circle.radius = radius;
			
			var intersection1:Vector2 = TEMP_VEC_1;
			var intersection2:Vector2 = TEMP_VEC_2;
			intersectionsWithCircle(circle, intersection1, intersection2);
			
			var angle1:Number = center.angleToPosition(intersection1);
			var angle2:Number = center.angleToPosition(intersection2);
			clipAtEndAngle(direction > 0 ? angle1 : angle2);
			
			ArcPool.__RELEASE__(TEMP_ARC_1);							// RELEASE ARC
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);						// RELEASE VECTOR2
			
			if(isNaN(circumference)){
					trace("It is nan");
			}	
		}
		
		
		
		
		[inline] final public function distForAngle(angle:Number):Number{
			return radius * angle;
		}
		
		
		
		[inline] final public function angleForDistance(distance:Number):Number{
			return startAngle + ((distance/radius) * direction);
		}
		

	}
		
}

