﻿package math {

	public class Circle {

		public static var temp1:Circle = new Circle(0, 0, 50);
		public static var temp2:Circle = new Circle(0, 0, 50);
		public static var temp3:Circle = new Circle(0, 0, 50);
		public static var temp4:Circle = new Circle(0, 0, 50);
		public static var temp5:Circle = new Circle(0, 0, 50);
		
		
		public var x:Number = 0;
		public var y:Number = 0;
		public var r:Number = 0;

		
		public function Circle(x:Number, y:Number, r:Number){
			this.x = x;
			this.y = y;
			this.r = r;
		}
		
	}
	
}
