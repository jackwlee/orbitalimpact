﻿package math {
	
	
	public class OrbitPath {

		public var firstArcNode:ArcNode;
		public startingDistance:Number;

		
		public function OrbitPath(startingDistance:Number) {
			this.startingDistance = startingDistance;
		}
	

		public function get length():Number{
			
		}
		
		
		public function get lastArcNode():ArcNode{
			var arcNode:ArcNode = firstArcNode;
			if(arcNode == null) return null;
			while(arcNode.next){
				arcNode = arcNode.next;
			}
			return arcNode;
		}
		

	}
	
}
