﻿package math {
	
	import math.Arc;
	import math.Circle;
	import components.OrbitTarget;
	
	
	public class PotentialArcs{
		
		public const clockwise:Arc = new Arc()
		public const counter:Arc = new Arc();
		public var target:OrbitTarget;
		
	}
	
}
