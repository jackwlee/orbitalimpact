﻿package math {
	
	import aspire.geom.Vector2;
	import utils.Vector2Pool;
	import utils.ArcPool;
	import utils.ArcPathSegmentPool;

	
	public class ArcPathSegment extends PathSegment{

		public var center:Vector2 = new Vector2();
		public var radius:Number;
		public var startAngle:Number;
		public var direction:Number;	// -1 or 1
		
	
		
		
		public function get circumference():Number{
			return Math.PI * radius * 2;
		}
		
		
		// Clips the end of the path at the specified angle
		public function clipAtEndAngle(angle:Number):void{
			if(next) throw(new Error("Cannot clip an ArcPathSegment which is not a tail"));
			
			angle *= direction;
			
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			var a1:Vector2 = TEMP_VEC_1.setToHeading(startAngle);
			var a2:Vector2 = TEMP_VEC_2.setToHeading(angle);

			var angleBetween:Number;
			
			if(direction > 0){
				angleBetween = Math.atan2(a1.cross(a2), a1.dot(a2));
			}else{
				angleBetween = Math.atan2(a2.cross(a1), a2.dot(a1));
			}
			
			if(angleBetween >= 0){
				segmentLength = angleBetween * radius;
			}else{
				segmentLength = (Math.PI * radius * 2) + (angleBetween * radius);
			}
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);						// RELEASE VECTOR2
		}
		
		
		// Assumes an overlap is occuring, and that the path started outside
		public function clipToIntersectionWithOtherArcPathSegment(other:ArcPathSegment):void{
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_3:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2

			var intersection1:Vector2 = TEMP_VEC_1;
			var intersection2:Vector2 = TEMP_VEC_2;
			intersectionsWithOtherArcPathSegment(other, intersection1, intersection2);
			
			var angle1:Number = center.angleToPosition(intersection1);
			var angle2:Number = center.angleToPosition(intersection2);
			clipAtEndAngle(direction > 0 ? angle1 : (Math.PI*2) - angle2);
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_3);						// RELEASE VECTOR2
			
			
			/*
			var TEMP_ARC_1:Arc = ArcPathSegmentPool.__GET__();			// GET ARC
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();				// GET VECTOR2
			
			var circle:Arc = TEMP_ARC_1;
			circle.center.x = x;
			circle.center.y = y;
			circle.radius = radius;
			
			var intersection1:Vector2 = TEMP_VEC_1;
			var intersection2:Vector2 = TEMP_VEC_2;
			intersectionsWithOtherArcPathSegment(circle, intersection1, intersection2);
			
			var angle1:Number = center.angleToPosition(intersection1);
			var angle2:Number = center.angleToPosition(intersection2);
			clipAtEndAngle(direction > 0 ? angle1 : angle2);
			
			ArcPathSegmentPool.__RELEASE__(TEMP_ARC_1);					// RELEASE ARC
			Vector2Pool.__RELEASE__(TEMP_VEC_1);						// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);						// RELEASE VECTOR2
			
			if(isNaN(circumference)){
				throw(new Error("Circumference is NaN"));
			}	
			*/
		}
		
		
		// Assumes an overlap is occuring
		public function intersectionsWithOtherArcPathSegment(other:ArcPathSegment, out1:Vector2, out2:Vector2) {
			var x1 = center.x;
			var y1 = center.y;
			var d1 = radius;
			
			var x2 = other.center.x;
			var y2 = other.center.y;
			var d2 = other.radius;

			// calculate the intersections 
			var a = d2;
			var b = d1;
			var c = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
			var d = (b*b+c*c-a*a)/(2*c);
			var h = Math.sqrt(b*b-d*d);
			
			out1.x = (x2-x1)*d/c + (y2-y1)*h/c +  x1;
			out1.y = (y2-y1)*d/c - (x2-x1)*h/c +  y1;
			out2.x = (x2-x1)*d/c - (y2-y1)*h/c +  x1;
			out2.y = (y2-y1)*d/c + (x2-x1)*h/c +  y1;
		}
		
		
	
		override public function clipStartDistance(distanceOffset:Number):void{
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();		// GET VECTOR2
			var TEMP_VEC_2:Vector2 = Vector2Pool.__GET__();		// GET VECTOR2
			
			var currentStartAngleVector:Vector2 = positionAtDistance(segmentStartDistance, TEMP_VEC_1);
			var newStartAngleVector:Vector2 = positionAtDistance(segmentStartDistance + distanceOffset, TEMP_VEC_2);
			
			var angleBetween:Number = Vector2.angleBetween(currentStartAngleVector, newStartAngleVector);
			startAngle += angleBetween * direction;
			segmentLength -= distanceOffset;
			offsetStartDistance(-distanceOffset);
			
			Vector2Pool.__RELEASE__(TEMP_VEC_1);				// RELEASE VECTOR2
			Vector2Pool.__RELEASE__(TEMP_VEC_2);				// RELEASE VECTOR2
		}

		
		
		override public function positionAtDistance(globalDistance:Number, out:Vector2):Vector2{
			if(distanceFallsOnPath(globalDistance) == false && segmentLength != circumference) return next ? next.positionAtDistance(globalDistance, out) : null;
			out.x = 0;
			out.y = radius;
			out.angle = perpAngleAtDistance(globalDistance);
			out.add(center);
			return out;
		}
		
		
	
		public function perpAngleAtDistance(globalDistance:Number):Number{
			return startAngle + (((globalDistance-segmentStartDistance)/radius)*direction);
		}
		
		
		
		override public function angleAtDistance(globalDistance:Number):Number{
			if(distanceFallsOnPath(globalDistance) == false && segmentLength != circumference) return next ? next.angleAtDistance(globalDistance) : null;
			var angle:Number = perpAngleAtDistance(globalDistance);
			if(direction > 0) angle += Math.PI/2;
			else angle -= Math.PI/2;
			angle += Math.PI/2;
			return angle;
		}
		
		
					
		public function distanceForAngle(angle:Number):Number{
			return radius * angle;
		}
		

		
	}
	
}
