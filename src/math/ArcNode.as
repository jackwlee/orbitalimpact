﻿package math {
	
	import components.OrbitTarget;
	import aspire.geom.Vector2;
	import utils.Vector2Pool;

	
	public class ArcNode {
		
		public var arc:Arc;

		public var startDistance:Number = 0;
		//public var startTime:Number;
		//public var endTime:Number = 0xffffff;
		
		public var next:ArcNode;
		public var prev:ArcNode;
		public var target:OrbitTarget;
		
		
		public function ArcNode(arc:Arc, target:OrbitTarget, startDistance:Number) {
			this.arc = arc;
			this.target = target;
			this.startDistance = startDistance;
			if(target){
				arc.radius = target.radius;
				arc.center.x = target.entity.position.x;
				arc.center.y = target.entity.position.y;
			}
		}
		

		public function clone():ArcNode{
			var arc:Arc = this.arc.clone();
			var arcNode:ArcNode = new ArcNode(arc, this.target, this.startDistance);
			if(this.next) arcNode.next = this.next.clone();
			return arcNode;
		}
		
		
		public function get length():Number{
			return arc.length;
		}
				
				
		public function get tail():ArcNode{
			var tail:ArcNode = this;
			while(tail.next){
				tail = tail.next;
			}
			return tail;
		}
		

		public function positionAtDistance(distance:Number, out:Vector2):Vector2{
			if(distance > startDistance + arc.length){
				if(next){
					return next.positionAtDistance(distance, out);
				}else{
					return null;
				}
			}else{
				return arc.positionAtDistance(distance-startDistance, out);
			}
		}
		
		
		public function attach(nextArcNode:ArcNode):ArcNode{
			if(nextArcNode == this) throw new Error("Cannot add self as next");
			//if(this.arc.length == 0xffffff) throw new Error("Created an arc node without first setting the length");
			next = nextArcNode;
			next.prev = this;

			var tail:ArcNode = this;
			while(tail.next){
				tail = tail.next;
			}
			
			return tail;
		}
		
		
		
		public function continueOnFromPrev():void{
			var TEMP_VEC_1:Vector2 = Vector2Pool.__GET__();
			
			var endOfPrev:Vector2 = prev.arc.positionAtDistance(prev.arc.length, TEMP_VEC_1);
			arc.startAngle = arc.center.angleToPosition(endOfPrev);
			Vector2Pool.__RELEASE__(TEMP_VEC_1);
		}
		
		
		[inline] final public function getNodeForDistanceForwards(distance:Number):ArcNode{
			if(next == null) return this;
			if(distance >= startDistance && distance < next.startDistance) return this;
			else return next.getNodeForDistanceForwards(distance);
		}
		
		
		
		[inline] final public function getNodeForDistanceBackwards(distance:Number):ArcNode{
			if(prev == null) return this;
			if(distance >= startDistance && distance < prev.startDistance) return this;
			else return prev.getNodeForDistanceBackwards(distance);
		}

	

	}
	
}
