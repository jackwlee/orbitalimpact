﻿package monsterz.game{
	
	import org.osflash.signals.Signal;
	
	
	public class Signals extends SignalHub{
		
		// Note, do not call dispatch on any of these as dispatching is handled by the framework
		// See the public methods in the Controller instead

		public var gameplayFinished		:Signal = newSignal(); // Notifies when the gameplay has finished
		public var gameplayStarted		:Signal = newSignal(); // Notifies when the gameplay has started
		
		public var pause				:Signal = newSignal(); // Notifies of pause
		public var unpause				:Signal = newSignal(); // Notifies of unpause
		
		public var activated			:Signal = newSignal(); // Notifies when the game is not in an intro / outro / or pause state
		public var deactivated			:Signal = newSignal(); // Notifies when the game is in either a an intro / outro / or pause state
		
		public var update				:Signal = newSignal(); // Notifies of normal update
		public var pauseableUpdate		:Signal = newSignal(); // Notifies of update when not paused
		
		public var timeLapsed			:Signal = newSignal(); // Notofies when the time has lapsed
		
		public var destroyGame			:Signal = newSignal(); // Notifies when destroying is happening
	
		internal var passed				:Signal = newSignal(); // Notifies when the game has passed
		internal var failed				:Signal = newSignal(); // Notifies when the game has failed
		internal var complete			:Signal = newSignal(); // When the game should complete and return to the parent framework

		//internal var introComplete	:Signal = newSignal(); // Intro animation has completed
		//internal var outroComplete	:Signal = newSignal(); // Outro animation has completed
		
		
		/////////////////////////////////////////////////////
		//
		//  Singleton
		//
		/////////////////////////////////////////////////////
		
		private static var _instance:Signals;
		
		public static function get instance():Signals{
			if(_instance == null){
				_instance = new Signals();
			}
			return _instance;
		}
		
		override protected function destroy(){
			reset();
			_instance = null;
		}

	}
	
}