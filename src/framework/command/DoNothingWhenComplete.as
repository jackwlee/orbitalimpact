﻿package  {
	
	import framework.command.CommandDelegate;
	import framework.command.ICommand;

	
	public class DoNothingWhenComplete implements CommandDelegate{

		
		public function completed(command:ICommand){}
		
		
	}
	
}
