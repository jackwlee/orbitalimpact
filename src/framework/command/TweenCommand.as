﻿package framework.command {
	
	import starling.animation.Tween;
	import starling.animation.Juggler;

	
	public class TweenCommand implements ICommand{

		public var tween:Tween;
		public var juggler:Juggler;
		
		
		public function TweenCommand(tween:Tween, juggler:Juggler) {
			this.tween = tween;
			this.juggler = juggler;
		}

		
		public function execute(delegate:CommandDelegate){
			CommandHub.instance.executed.dispatch(this);
			tween.onComplete = delegate.completed;
			tween.onCompleteArgs = [this];
			juggler.add(tween);
			CommandHub.instance.completed.dispatch(this);
		}

	}
	
}
