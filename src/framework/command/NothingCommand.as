﻿package framework.command {

	
	public class NothingCommand implements ICommand {

		
		public function execute(delegate:CommandDelegate){
			CommandHub.instance.executed.dispatch(this);
			delegate.completed(this);
			CommandHub.instance.completed.dispatch(this);
		}

	}
	
}
