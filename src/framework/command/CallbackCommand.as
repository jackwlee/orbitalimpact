﻿package framework.command {
	
	import monsterz.game.ICommand;
	import framework.command.CommandDelegate;

	
	public class CallbackCommand implements ICommand{
	
		private var delegate:CommandDelegate;
		private var callback:Function;

		
		public function CallbackCommand(callback:Function){
			this.callback = callback;
		}

		
		//
		// ICommand
		//
		public function execute(delegate:CommandDelegate){
			CommandHub.instance.executed.dispatch(this);
			callback();
			delegate.completed(this);
			CommandHub.instance.completed.dispatch(this);
		}
		
	}
	
}
