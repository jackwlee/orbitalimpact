﻿package framework.command {
	
	import starling.animation.Tween;
	import starling.animation.Juggler;
	
	
	public class TransitionCommand extends TweenCommand {
		
		
		public function TransitionCommand(tween:Tween, juggler:Juggler) {
			super(tween, juggler);
		}
		
	}

	
}
