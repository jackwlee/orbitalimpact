﻿package framework.command {
	
	public class NoTransitionCommand extends TransitionCommand{

		
		public function NoTransitionCommand() {
			super(null, null);
		}

		
		override public function execute(delegate:CommandDelegate){
			CommandHub.instance.executed.dispatch(this);
			delegate.completed(this);
			CommandHub.instance.completed.dispatch(this);
		}

	}
	
}
