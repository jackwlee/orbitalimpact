﻿package framework.command {
	
	import framework.command.TransitionCommand;
	import framework.ui.Screen;
	import starling.animation.Tween;
	import starling.animation.Transitions;

	
	public class SlideOutTransition extends TransitionCommand {

		public var reverse:Boolean;

		
		public function SlideOutTransition(screen:Screen, reverse:Boolean){
			this.reverse = reverse;
			
			var tween:Tween = new Tween(screen, 0.5, Transitions.EASE_IN);
			tween.animate("x", reverse ? -1024/2 : 1024/2);
			super(tween, Jugglers.instance.ui);
		}

	}
	
}
