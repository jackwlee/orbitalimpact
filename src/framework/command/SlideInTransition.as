﻿package framework.command {
	
	import framework.command.TransitionCommand;
	import framework.ui.Screen;
	import starling.animation.Tween;
	import starling.animation.Transitions;

	
	public class SlideInTransition extends TransitionCommand {

		
		public function SlideInTransition(screen:Screen, reverse:Boolean){
			var tween:Tween = new Tween(screen, 0.5, Transitions.EASE_OUT);
			tween.animate("x", 0);
			screen.x = reverse ? 1024/2 : -1024/2;
			super(tween, Jugglers.instance.ui);
		}

	}
	
}
