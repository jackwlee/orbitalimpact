﻿package framework.command {
	
	public interface ICommand {
		
		function execute(delegate:CommandDelegate);

	}
	
}
