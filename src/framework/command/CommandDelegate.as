﻿package framework.command {
	
	public interface CommandDelegate {

		function completed(command:ICommand);

	}
	
}
