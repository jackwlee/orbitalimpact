﻿package framework.command {

	import flump.display.LibraryLoader;
	import flump.executor.Future;
	import flump.display.Library;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flump.executor.FutureTask;
	import monsterz.game.ICommand;
	
	
	public class CommandSeries implements ICommand, CommandDelegate{

		private var delegate:CommandDelegate;
		private var pending:Vector.<ICommand> = new Vector.<ICommand>();
		
		
		public function CommandSeries(){
		}
		
		
		public function add(command:ICommand):void{
			pending.push(command);
		}
		
		
		private function next(){
			if(pending.length == 0){
				delegate.completed(this);
				CommandHub.instance.completed.dispatch(this);
			}else{
				pending[0].execute(this);
			}
		}
		
		
		//
		// ICommand
		//
		public function execute(delegate:CommandDelegate){
			CommandHub.instance.executed.dispatch(this);
			this.delegate = delegate;
			next();
		}
		
		//
		// CommandDelegate
		//
		public function completed(command:ICommand){
			pending.shift();
			next();
		}

	}
	
}
