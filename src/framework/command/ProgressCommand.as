﻿package framework.command {
	
	import monsterz.game.ICommand;
	import framework.command.CommandDelegate;

	
	public class ProgressCommand implements ICommand{
	
		private var delegate:CommandDelegate;
		private var triggered:Boolean = false;
		private var executed:Boolean = false;

		
		public function trigger(){
			if(triggered && executed){
				delegate.completed(this);
				CommandHub.instance.completed.dispatch(this);
			}
			
			triggered = true;
		}
		
		//
		// ICommand
		//
		public function execute(delegate:CommandDelegate){
			this.delegate = delegate;
			CommandHub.instance.executed.dispatch(this);
			executed = true;
			trigger();
		}
		
	}
	
}
