﻿package framework.command {

	
	public class CallbackWhenComplete implements CommandDelegate{

		private var callback:Function;
		
		
		public function CallbackWhenComplete(callback:Function){
			this.callback = callback;
		}
		
		
		public function completed(command:ICommand){
			callback();
		}
		
		
	}
	
}
