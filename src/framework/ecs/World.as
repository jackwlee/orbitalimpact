﻿package framework.ecs {
	
	public class World {

		private var entities:Vector.<Entity> = new Vector.<Entity>();
		private var collections:Vector.<EntityCollection> = new Vector.<EntityCollection>();
		
		
		public function isAdded(entity:Entity):Boolean{
			var index:int = entities.indexOf(entity);
			return index != -1;
		}
		

		
		public function isSubscribed(collection:EntityCollection){
			var index:int = collections.indexOf(collection);
			return index != -1;
		}
		
		
		
		public function subscribe(collection:EntityCollection){
			if(isSubscribed(collection)) throw new Error("collection " + collection + " is already subscribed.");
			collections.push(collection);
			
			for each(var entity:Entity in entities){
				collection.onEntityAdded(entity);
				entity.added.dispatch(entity);
			}
		}
		
		
		
		public function unsubscribe(collection:EntityCollection){
			if(isSubscribed(collection) == false) throw new Error("collection " + collection + " is not subscribed. Cannot remove.");
			var index:int = collections.indexOf(collection);
			collections.splice(index, 1);
		}
		
		
		
		public function add(entity:Entity):void{
			if(isAdded(entity)) throw new Error("Entity " + entity + " is already added");
			entities[entities.length] = entity; // Optimization
			entity.generateDescription();
			
			for each(var collection:EntityCollection in collections){
				collection.onEntityAdded(entity);
				entity.added.dispatch(entity);
			}
		}
		
		
		
		public function remove(entity:Entity):void{
			if(isAdded(entity) == false) throw new Error("Entity " + entity + " was not added. Cannot remove");
			
			var index:int = entities.indexOf(entity);
			entities.splice(index, 1);
			
			for each(var collection:EntityCollection in collections){
				collection.onEntityRemoved(entity);
				entity.removed.dispatch(entity);
			}
		}
		
		
		
		//
		// Singelton
		//
		private static var _instance:World;
	
		public static function get instance():World{
			if(_instance == null) _instance = new World();
			return _instance;
		}

		
	}
	
}
