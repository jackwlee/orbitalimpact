﻿package framework.ecs {
	
	import flash.geom.Point;
	import org.osflash.signals.Signal;
	
	
	public class EntityCollection {

		private var _entities:Vector.<Entity> = new Vector.<Entity>();
		private var types:Array = new Array();
		
		public const added:Signal = new Signal(Entity);
		public const removed:Signal = new Signal(Entity);
		

		public function get entities():Vector.<Entity>{
			return _entities;
		}
		
		
		
		public function EntityCollection(types:Array){
			this.types = types;
			World.instance.subscribe(this);
		}

		
		
		private function hasTypes(entity:Entity):Boolean{
			for each(var klass:Class in types){
				if(entity.hasComponent(klass) == false) return false;
			}
			return true;
		}
		
		
		
		final internal function onEntityAdded(entity:Entity){
			if(hasTypes(entity)){
				entities.push(entity);
				added.dispatch(entity);
			}
		}

		
		
		final internal function onEntityRemoved(entity:Entity){
			if(hasTypes(entity)){
				var index:int = entities.indexOf(entity);
				entities.splice(index, 1);
				removed.dispatch(entity);
			}
		}
		
	
	}
	
}
