﻿package framework.ecs {

	import starling.display.Sprite;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import starling.animation.IAnimatable;
	import components.Position;
	import components.Velocity;
	import components.Display;
	import org.osflash.signals.Signal;
	
	
	public class Entity implements IComponent{
	
		private static const qualifiedIComponentName:String = getQualifiedClassName(IComponent);
		private static const typeLookup:Dictionary = new Dictionary();
		
		private var componentLookup:Dictionary;
		
		public var display:Display = new Display();
		public var position:Position = new Position();
		public var velocity:Velocity = new Velocity();
		
		public var klass:Class;
		
		public const added:Signal = new Signal(Entity);
		public const removed:Signal = new Signal(Entity);
		
		
		
		internal function generateDescription(){
			var entityKlassName:String = getQualifiedClassName(this);
			
			if(typeLookup[entityKlassName] == undefined){
				var entityClass:Class = getDefinitionByName(entityKlassName) as Class;
				klass = entityClass;
				
				trace("Registered Entity: " + entityClass);
				
				var dict:Dictionary = new Dictionary();
				typeLookup[entityKlassName] = dict;
				trace("Registered Component: " + entityClass);
				dict[entityClass] = "self";
				
				var description:XML = describeType(this);
				for each(var v:XML in description.variable){
					var typeString:String = v.@type;
					var componentklass:Class = getDefinitionByName(typeString) as Class;
					var varDescription:XML = describeType(componentklass);
					for each(var interfaceImp:XML in varDescription.factory.implementsInterface){
						if(interfaceImp.@type == qualifiedIComponentName){
							trace("Registered Component: " + componentklass);
							dict[componentklass] = v.@name;
						}
					}
				}
			}
			
			componentLookup = typeLookup[entityKlassName];
		}
		
		

		
		public function render(){
		}
		
		
		
		private function get self():Entity{
			return this;
		}
		
		
		
		public function hasComponent(klass:Class):Boolean{
			return componentLookup[klass] != undefined;// || this.klass == klass;
		}
		
		
		
		public function getComponent(klass:Class):IComponent{
			return this[componentLookup[klass]];
		}
		
		
	}
	
}




