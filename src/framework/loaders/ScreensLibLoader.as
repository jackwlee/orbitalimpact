﻿package framework.loaders {

	import flump.display.LibraryLoader;
	import flump.executor.Future;
	import flump.display.Library;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flump.executor.FutureTask;
	
	import framework.flump.FlumpLibs;
	import framework.command.*;
	
	
	public class ScreensLibLoader implements ICommand, CommandDelegate{

		private var delegate:CommandDelegate;
		private var libLoader:LibLoader;
		
		
		public function ScreensLibLoader(){}
		
		//
		// ICommand
		//
		public function execute(delegate:CommandDelegate){
			this.delegate = delegate;
			if(FlumpLibs.instance.screens == null){
				libLoader = new LibLoader("Screens");
				libLoader.execute(this);
			}else{
				delegate.completed(this);
			}
		}
		
		//
		// CommandDelegate
		//
		public function completed(command:ICommand){
			FlumpLibs.instance.screens = libLoader.result;
			delegate.completed(this);
		}

	}
	
}
