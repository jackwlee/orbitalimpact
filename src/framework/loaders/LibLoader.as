﻿package framework.loaders {

	import flump.display.LibraryLoader;
	import flump.executor.Future;
	import flump.display.Library;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flump.executor.FutureTask;
	
	import framework.command.*;
	
	
	public class LibLoader implements ICommand{

		private var delegate:CommandDelegate;
		private var libId:String;
		private var loader:LibraryLoader;
		
		private var path:String;
		
		public var result:Library;
		
		
		public function LibLoader(libId:String){
			this.libId = libId;
		}
		
		
		public function execute(delegate:CommandDelegate){
			this.delegate = delegate;
			loader = new LibraryLoader();
			path = "assets/flump/scale_"+ Globals.instance.scaleFactor + "/" + libId + ".zip";
			trace("Loading: " + path);
			var future:Future = loader.loadURL(path);
			future.completed.connect(onLibraryLoaded);
			future.failed.connect(onLibraryLoadFailed);
		}
		
		
		private function onLibraryLoaded(future:FutureTask){
			var lib:Library = future.result;
			result = lib;
			trace("Load complete: " + path);
			delegate.completed(this);
		}
		
		
		private function onLibraryLoadFailed(errorEvent:IOErrorEvent){
			throw(errorEvent.text);
		}

	}
	
}
