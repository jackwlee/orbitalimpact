﻿package framework.loaders {

	import flump.display.LibraryLoader;
	import flump.executor.Future;
	import flump.display.Library;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flump.executor.FutureTask;
	
	import framework.flump.FlumpLibs;
	import framework.command.*;
	
	
	public class GameLibLoader implements ICommand, CommandDelegate{

		private var delegate:CommandDelegate;
		private var libLoader:LibLoader;
		
		
		public function GameLibLoader(){}
		
		
		//
		// ICommand
		//
		public function execute(delegate:CommandDelegate){
			this.delegate = delegate;
			if(FlumpLibs.instance.game == null){
				libLoader = new LibLoader("Game");
				libLoader.execute(this);
			}else{
				delegate.completed(this);
			}
		}

		
		//
		// CommandDelegate
		//
		public function completed(command:ICommand){
			FlumpLibs.instance.game = libLoader.result;
			delegate.completed(this);
		}

	}
	
}
