﻿package framework.ui {
	
	import starling.display.Image;
	import framework.*;
	import framework.ui.*;
	import framework.ui.button.*;
	import framework.flump.*;
	import framework.command.*;
	import framework.loaders.*;
	import flump.display.*;
	import starling.display.*;
	import starling.text.*;
	import starling.events.*;
	import starling.animation.*;
	import fl.transitions.Tween;

	
	public class StarField extends Sprite{

		public var xPos:Number = 0;
		private var prevX:Number = 0;
		private var xVel:Number = 0;
		
			
		public var stars:Vector.<Image> = new Vector.<Image>();
		
		
		public function StarField() {
			for(var i:int = 0; i < 100; i++){
				var star = FlumpLibs.instance.screens.createImage("Star");
				star.x = Math.random() * 1024/2;
				star.y = Math.random() * 768/2;
				star.scaleX = Math.random();
				star.scaleY = star.scaleX;
				addChild(star);
				stars.push(star);
				star.alpha = (Math.random() + 1) / 6;
			}
			
			CommandHub.instance.executed.subscribe(SlideOutTransition, slide);
		}
		
		
		private function slide(command:SlideOutTransition){
			var tween:Tween = new Tween(this, 1, Transitions.EASE_IN_OUT);
			tween.animate("xPos", xPos + 100);
			tween.onUpdate = update;
			tween.onComplete = complete;
			Jugglers.instance.ui.add(tween);
			
			for each(var star:DisplayObject in stars){
				if(command.reverse) star.pivotX = 2;
				else star.pivotX = star.width / star.scaleX;
			}
		}
		
		
		private function complete(){
			xVel = 0;
			update();
		}
		
		
		private function update(){
			xVel = xPos - prevX;
			for each(var star:DisplayObject in stars){
				star.scaleX = star.scaleY + (xVel * 1 * star.scaleY * star.scaleY);
				star.x -= xVel * 8 * (star.scaleY*star.scaleY);
				if(star.x < 0){
					star.x += 1024/2;
					star.y = Math.random() * 768/2;
				}
				if(star.x > 1024/2){
					star.x -= 1024/2;
					star.y = Math.random() * 768/2;
				}				
			}
			prevX = xPos;
		}

		
	}

	
}
