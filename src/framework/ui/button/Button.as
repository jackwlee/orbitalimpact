﻿package framework.ui.button{
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import org.osflash.signals.Signal;
	
	
	public class Button{
		
		public var display:DisplayObject;
		private var delegate:ButtonDelegate;
		private var displayDelegate:ButtonDisplayDelegate;
		
		public var isPressed:Boolean = false;
		private var _enabled:Boolean = true;
		
		public const pressed:Signal = new Signal();
		public const released:Signal = new Signal();
		
		
		public function Button(display:DisplayObject, delegate:ButtonDelegate, displayDelegate:ButtonDisplayDelegate = null){
			if(displayDelegate == null) displayDelegate = new StandardButtonDisplay();
			this.display = display;
			this.delegate = delegate;
			this.displayDelegate = displayDelegate;
			
			displayDelegate.init(display);
			
			display.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			display.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		
		public function set enabled(value:Boolean):void{
			if(enabled && isPressed && value == false){
				isPressed = false;
				displayDelegate.animateCancel();
			}
			_enabled = value;
			displayDelegate.setEnabled(value);
		}
		
		
		public function get enabled():Boolean{
			return _enabled;
		}
		
		
		private function onTouch(e:TouchEvent):void{
			var touch:Touch = e.touches[0];
			switch(touch.phase){
				//
				case TouchPhase.BEGAN:
					if(delegate.buttonIsEnabled(this) && !pressed == false && enabled){
						isPressed = true;
						displayDelegate.animateTouchDown();
						pressed.dispatch();
					}
					break;
				//
				case TouchPhase.MOVED:
					if(!delegate.buttonIsEnabled(this) && isPressed){
						isPressed = false;
						displayDelegate.animateCancel();
					}
					break;
				//
				case TouchPhase.HOVER:
					if(!delegate.buttonIsEnabled(this) && isPressed){
						isPressed = false;
						displayDelegate.animateCancel();
					}
					break;
				//
				case TouchPhase.STATIONARY:
					if(!delegate.buttonIsEnabled(this) && isPressed){
						isPressed = false;
						displayDelegate.animateCancel();
					}
					break;
				//
				case TouchPhase.ENDED:
					if(delegate.buttonIsEnabled(this) && isPressed && enabled){
						isPressed = false;
						displayDelegate.animateTouchRelease();
						released.dispatch();
					}
					break;
			}
			
			if(delegate.buttonIsEnabled(this)) e.stopImmediatePropagation();
		}
		

		public function destroy(){
			display.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			display.removeEventListener(TouchEvent.TOUCH, onTouch);
		}

		
		private function onRemovedFromStage(e:Event):void{
			destroy();
		}
		
	}
	
}