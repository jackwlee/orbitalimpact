﻿package framework.ui.button{
	
	import starling.display.DisplayObject;
	
	
	public class StandardButtonDisplay implements ButtonDisplayDelegate{
		
		private var display:DisplayObject;
		
		
		public function init(display:DisplayObject):void{
			this.display = display;
		}
		
		
		public function animateTouchDown():void{
			display.scaleX = 0.75;
			display.scaleY = 0.75;
		}
		
		
		public function animateTouchRelease():void{
			display.scaleX = 1;
			display.scaleY = 1;
		}
		
		
		public function animateCancel():void{
			display.scaleX = 1;
			display.scaleY = 1;
		}
		
		
		public function setEnabled(value:Boolean):void{
			if(value) display.alpha = 1;
			else display.alpha = 0.5;
		}
		
	}
	
}