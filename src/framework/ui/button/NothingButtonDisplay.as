﻿package framework.ui.button{
	
	import starling.display.DisplayObject;
	
	
	public class NothingButtonDisplay implements ButtonDisplayDelegate{
		
		
		public function init(display:DisplayObject):void{
		}
		
		
		public function animateTouchDown():void{
		}
		
		
		public function animateTouchRelease():void{
		}
		
		
		public function animateCancel():void{
		}
		
		
		public function setEnabled(value:Boolean):void{
		}
		
	}
	
}
