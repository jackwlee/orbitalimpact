﻿package framework.ui.button{

	import starling.display.DisplayObject;
	
	
	public interface ButtonDisplayDelegate{
		
		function init(display:DisplayObject):void;
		function animateTouchDown():void;
		function animateTouchRelease():void;
		function animateCancel():void;
		function setEnabled(value:Boolean):void;
		
	}
}