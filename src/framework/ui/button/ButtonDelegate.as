﻿package framework.ui.button{
	
	
	public interface ButtonDelegate{
		
		function buttonIsEnabled(button:Button):Boolean;
			
	}
	
	
}