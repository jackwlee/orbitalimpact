﻿package framework.ui.button{
	
	import flump.display.Movie;
	import starling.display.DisplayObject;
	
	
	public class MovieButtonDisplay implements ButtonDisplayDelegate{
		
		
		private var movie:Movie;;
		
		
		public function init(display:DisplayObject):void{
			this.movie = display as Movie;
		}
		
		
		public function animateTouchDown():void{
			movie.goTo(1);
		}
		
		
		public function animateTouchRelease():void{
			movie.goTo(0);
		}
		
		
		public function animateCancel():void{
			movie.goTo(0);
		}
		
		
		public function setEnabled(value:Boolean):void{
		}
		
	}
	
}