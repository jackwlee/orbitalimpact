﻿package framework.ui {
	
	import framework.*;
	import framework.ui.*;
	import framework.ui.button.*;
	import framework.flump.*;
	import framework.command.*;
	import framework.loaders.*;	
	import entities.*;
	import flump.display.*;
	import starling.display.Button;
	import starling.text.*;
	import starling.events.*;
	import starling.animation.*;
	import starling.display.DisplayObject;
	import framework.ecs.EntityCollection;
	import flash.geom.Point;
	import systems.MovementSystem;
	import systems.DisplaySystem;
	import systems.OrbitSystem;
	
	
	public class ScreenMain extends Screen implements ButtonDelegate{
	
		private var display:Movie;
		
		private var btnBattle:Button;
		private var btnTraining:Button;
		private var btnOptions:Button;

		
		public function ScreenMain(){
			loadCommands.add(new ScreensLibLoader());
		}
		
		
		private function get lib():Library{
			return FlumpLibs.instance.screens;
		}
		
		
		override public function loaded(){
			display = lib.createMovie("ScreenMain");
			addChild(display);

			btnBattle = new Button(display.getChildByName("BtnBattle"), this, new MovieButtonDisplay());
			btnTraining = new Button(display.getChildByName("BtnTraining"), this, new MovieButtonDisplay());
			btnOptions = new Button(display.getChildByName("BtnOptions"), this, new MovieButtonDisplay());
			
			btnBattle.released.add(goToBattle);
			btnTraining.released.add(goToTraining);
			btnOptions.released.add(goToOptions);
			
		}
		
		
		private function goToBattle(){
			ScreenManager.instance.change( new ScreenCharacterSelection() );
		}
		
		
		private function goToTraining(){
			
		}
		
		
		private function goToOptions(){
			
		}
	
		
		
		//
		// Button Delegate
		//
		public function buttonIsEnabled(button:Button):Boolean{
			return active;
		}
		
		
	}
	
}
