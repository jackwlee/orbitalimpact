﻿package framework.ui {

	public class ScreenTransition {
	
		public var from:Class;
		public var to:Class;
		public var transitionOut:Function; // Screen -> ICommand
		public var transitionIn:Function; // Screen -> ICommand
	
		
		public function ScreenTransition(from:Class, to:Class, transitionOut:Function, transitionIn:Function){
			this.from = from;
			this.to = to;
			this.transitionOut = transitionOut;
			this.transitionIn = transitionIn;
		}
		
	}
	
}
