﻿package framework.ui {
	
	import framework.ui.Screen;
	import framework.command.*;
	import starling.display.Sprite;
	
	
	public class ScreenManager extends Sprite implements CommandDelegate{

		
		private var current:Screen;
		private var commands:CommandSeries;
		
		
		public function ScreenManager(){
			commands = new CommandSeries();
		}
		
		
		public function change(newScreen:Screen){

			var exitCommands:CommandSeries = new CommandSeries();
			var enterCommands:CommandSeries = new CommandSeries();
			
			var transition:ScreenTransition = ScreenTransitions.instance.getTransitions(current, newScreen);
			
			
			if(current != null){				
				current.active = false;
				exitCommands.add(current.exit());
				exitCommands.add(transition.transitionOut(current));
				exitCommands.add(new CallbackCommand(function(){ Jugglers.instance.ui.remove(current.juggler) }));
				exitCommands.add(new CallbackCommand(function(){ removeChild(current) }));
			}
			
			enterCommands.add(newScreen.loadCommands);
			enterCommands.add(new CallbackCommand(newScreen.loaded));
			enterCommands.add(new CallbackCommand(function(){ addChild(newScreen) }));
			enterCommands.add(newScreen.enter());
			enterCommands.add(new CallbackCommand(function(){ Jugglers.instance.ui.add(newScreen.juggler) }));
			enterCommands.add(new CallbackCommand(function(){ newScreen.juggler.add(newScreen) }));
			enterCommands.add(transition.transitionIn(newScreen));
			enterCommands.add(new CallbackCommand(function(){
				current = newScreen;
				current.active = true;
			}));

			commands.add(exitCommands);
			commands.add(enterCommands);
			
			commands.execute(this);
		}
		
		
		
		//
		// CommandDelegate
		//
		public function completed(command:ICommand){
		}
		

		//
		// Singelton stuff
		//
		private static var _instance:ScreenManager;
		
		public static function get instance():ScreenManager{
			if(_instance == null) _instance = new ScreenManager();
			return _instance;
		}

		
	}
	
}
