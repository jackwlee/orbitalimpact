﻿package framework.ui {
	
	import framework.*;
	import framework.ui.*;
	import framework.ui.button.*;
	import framework.flump.*;
	import framework.command.*;
	import framework.loaders.*;
	import flump.display.*;
	import starling.display.*;
	import starling.text.*;
	import starling.events.*;
	import starling.animation.*;

	
	public class ScreenCharacterSelection extends Screen implements ButtonDelegate{

		private var display:Movie;
		
		private var btnNext:Button;
		
		
		override public function loaded(){
			display = FlumpLibs.instance.screens.createMovie("ScreenCharacterSelection");
			addChild(display);
			
			btnNext = new Button(display.getChildByName("BtnNext"), this, new MovieButtonDisplay());
			btnNext.released.add(goToWorldScreen);
		}
		
		
		private function goToWorldScreen(){
			ScreenManager.instance.change( new ScreenSelectWorld() );
		}
		
		
		//
		// Button delegate
		//
		public function buttonIsEnabled(button:Button):Boolean{
			return active;
		}

		
	}
	
}
