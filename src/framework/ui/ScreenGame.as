﻿package framework.ui {
	
	import entities.Rocket;
	import systems.OrbitSystem;
	import systems.MovementSystem;
	import systems.DisplaySystem;
	import framework.ecs.World;
	import entities.Planet;
	import systems.DebugSystem;
	import starling.events.KeyboardEvent;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.core.Starling;
	import starling.events.TouchPhase;
	import aspire.util.Arrays;
	import systems.Systems;
	import services.Time;
	import math.PotentialArcs;
	import math.Arc;
	import math.ArcNode;
	import framework.loaders.GameLibLoader;
	import framework.loaders.UILibLoader;
	import framework.loaders.ScreensLibLoader;
	import entities.ItemBox;
	
	
	public class ScreenGame extends Screen{
		
		private var rockets:Vector.<Rocket> = new Vector.<Rocket>();


		public function ScreenGame(){
			Systems.instance;
			
			loadCommands.add(new GameLibLoader());
			loadCommands.add(new UILibLoader());
			loadCommands.add(new ScreensLibLoader());
		}
		
		
		override public function loaded(){
			addChild(Layers.instance);
			var rocket:Rocket;
			
			var planets:Vector.<Planet> = new Vector.<Planet>();
			
			
			var planet:Planet = new Planet();
			planet.position.x = -200/2;
			planet.position.y = -200/2;
			planets.push(planet);
			
			var lastPlanet:Planet = planet;
			
			rocket = new Rocket();
			//rocket.orbit.createdOn = 0;
			rocket.position.x = planet.position.x + planet.orbitTarget.radius;
			rocket.position.y = planet.position.y;
			rocket.orbit.setAroundTarget(planet.orbitTarget);
			//rockets.push(rocket);
			
			//rocket.orbit.newArcNode(planet.orbitTarget, 1, 0);
			//var node:ArcNode = rocket.orbit.newArcNode(planet.orbitTarget);
			/*
			var startArc:Arc = node.arc;
			startArc.radius = planet.orbitTarget.radius;
			startArc.center.x = planet.position.x;
			startArc.center.y = planet.position.y;
			startArc.startAngle = 0;
			startArc.direction = -1;
			*/
			
			planet = new Planet();
			planet.position.x = 230/2;
			planet.position.y = -190/2;
			planets.push(planet);
			
			/*
			planet = new Planet();
			planet.position.x = 220/2;
			planet.position.y = 220/2;
			//planet.orbitTarget.radius = 20;
			planets.push(planet);
			*/
			
			planet = new Planet();
			planet.position.x = -210/2;
			planet.position.y = 130/2;
			planets.push(planet);
			
			var item:ItemBox = new ItemBox();
			


			for(var i:int = 0; i < planets.length; i++){
				for(var j:int = i + 1; j < planets.length; j++){
					var planet1:Planet = planets[i];
					var planet2:Planet = planets[j];
					planet1.orbitTarget.adjacent.push(planet2.orbitTarget);
					planet2.orbitTarget.adjacent.push(planet1.orbitTarget);
				}
			}
		
			/*
			rocket = new Rocket();
			rocket.position.x = planet.position.x + planet.orbitTarget.radius;
			rocket.position.y = planet.position.y;
			rockets.push(rocket);
			
			var node:ArcNode = rocket.orbit.newArcNode(planet.orbitTarget);
			var startArc:Arc = node.arc;
			startArc.radius = planet.orbitTarget.radius;
			startArc.center.x = planet.position.x;
			startArc.center.y = planet.position.y;
			startArc.startAngle = 0;
			startArc.direction = -1;
			*/
			
			
			/*
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent){
				//var pArcs:PotentialArcs = rocket.orbit.getPonentialArcs();
				//if(pArcs){
				//	rocket.orbit.newCourse(pArcs);
				//}
			});
			*/

			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, function (e:KeyboardEvent){
				trace(e.keyCode)
				switch(e.keyCode){
					//
					//case 32: rocket.orbit.attemptToSetNewHeading(); break;
					//
					//case 17: rocket.fireBullet(); break;
					//
					//case 90: rocket.plantMine(); break;
				}
				
			})

			
			Starling.current.stage.addEventListener(TouchEvent.TOUCH, function (e:TouchEvent){
				for each(var touch:Touch in e.touches){
					switch(touch.phase){
						//
						case TouchPhase.BEGAN:
							//rocket.orbit.attemptToSetNewHeading();
						break;
					}
				}
			});
			
			/*
			function move(posX:Number, posY:Number){
				for each(var rocket:Rocket in rockets){
					rocket.orbit.distanceTravelled = posX * 2;
				}
			}
			*/
		}
		
			
			
		override public function advanceTime(time:Number):void{
			var debugSystem:DebugSystem = Systems.instance.get(DebugSystem) as DebugSystem;
			debugSystem.graphics.clear();
			
			Systems.instance.advanceTime(time);
			Jugglers.instance.game.advanceTime(time);
			
			//for each(var rocket:Rocket in rockets) rocket.orbit.distanceTravelled += time * 100;
		}
		
	
		

	}
	
}
