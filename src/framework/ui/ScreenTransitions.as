﻿package framework.ui {
	
	import starling.animation.Tween;
	import framework.command.*;
	import framework.ui.Screen;
	import starling.core.Starling;
	import monsterz.game.Jugglers;
	import starling.animation.Transitions;
	
	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import org.osflash.signals.Signal;
	

	
	public class ScreenTransitions {

		private var transitions:Vector.<ScreenTransition> = new Vector.<ScreenTransition>();
		private var noTransition:ScreenTransition = new ScreenTransition( null, null, doNothing(), doNothing() );
			
		
		public function ScreenTransitions(){
			
			// ScreenMain <-> ScreenCharacterSelection
			transitions.push( new ScreenTransition( ScreenMain, ScreenCharacterSelection, slideOut(true), slideIn(true) ));
			transitions.push( new ScreenTransition( ScreenCharacterSelection, ScreenMain, slideOut(false), slideIn(false) ));
			
			// ScreenCharacterSelection <-> ScreenWorld
			transitions.push( new ScreenTransition( ScreenCharacterSelection, ScreenSelectWorld, slideOut(true), slideIn(true) ));
			transitions.push( new ScreenTransition( ScreenSelectWorld, ScreenCharacterSelection, slideOut(false), slideIn(false) ));
			
		}
		
		
		
		public function getTransitions(from:Screen, to:Screen):ScreenTransition{
			var fromKlass:Class = from == null ? null : getDefinitionByName( getQualifiedClassName(from) ) as Class;
			var toKlass:Class = to == null ? null : getDefinitionByName( getQualifiedClassName(to) ) as Class;
			
			for each(var screenTransition:ScreenTransition in transitions){
				if(screenTransition.from == fromKlass && screenTransition.to == toKlass){
					return screenTransition;
				}
			}
			
			return noTransition;
		}
		
		
		
		private function doNothing():Function{
			return function(screen:Screen):TransitionCommand{
				return new NoTransitionCommand();
			}
		}
		
		
		
		private function slideIn(reverse:Boolean):Function{
			return function(screen:Screen):TransitionCommand{
				return new SlideInTransition(screen, reverse);
			}
		}
		
		private function slideOut(reverse:Boolean):Function{
			return function(screen:Screen):TransitionCommand{
				return new SlideOutTransition(screen, reverse);
			}
		}
		
		
		
		//
		// Singleton
		//		
		private static var _instance:ScreenTransitions;
		
		public static function get instance():ScreenTransitions{
			if(_instance == null) _instance = new ScreenTransitions();
			return _instance;
		}
		
		
	}

	
}




