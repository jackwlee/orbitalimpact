﻿package framework.ui {
	
	import flump.display.Movie;
	import framework.command.*;
	import starling.display.Sprite;
	import starling.animation.Juggler;
	import starling.animation.IAnimatable;
	
	
	public class Screen extends Sprite implements CommandDelegate, IAnimatable {

		public var loadCommands:CommandSeries;
		public var active:Boolean = false;
		public var juggler:Juggler = new Juggler();
		
		
		public function Screen() {
			loadCommands = new CommandSeries();
		}
		
		
		public function loaded(){}
		
		
		public function beginLoading(){
			loadCommands.execute(this);
		}
		

		public function exit():ICommand{
			return new NothingCommand();
		}

		
		public function enter():ICommand{
			return new NothingCommand();
		}
		
		
		public function advanceTime(time:Number):void{
		}
		
		//
		// Command Delegate
		//
		public function completed(command:ICommand){
		}
		
		
	}
	
}
