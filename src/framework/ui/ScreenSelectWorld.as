﻿package framework.ui {
	
	import framework.*;
	import framework.ui.*;
	import framework.ui.button.*;
	import framework.flump.*;
	import framework.command.*;
	import framework.loaders.*;
	import flump.display.*;
	import starling.display.*;
	import starling.text.*;
	import starling.events.*;
	import starling.animation.*;

	
	public class ScreenSelectWorld extends Screen implements ButtonDelegate{

		private var display:Movie;
		
		
		
		override public function loaded(){
			display = FlumpLibs.instance.screens.createMovie("ScreenSelectWorld");
			addChild(display);
		}
		
		
		//
		// Button delegate
		//
		public function buttonIsEnabled(button:Button):Boolean{
			return active;
		}

		
	}
	
}
