﻿package framework.flump {
	import flump.display.Library;
	import flash.utils.Dictionary;
	
	
	public class FlumpLibs {

		private static var _instance:FlumpLibs;
		
		private var libs:Dictionary = new Dictionary();
		
		public var game:Library;
		public var ui:Library;
		public var screens:Library;

		
		public static function get instance():FlumpLibs{
			if(_instance == null) _instance = new FlumpLibs();
			return _instance;
		}

	}
	
}
